jQuery(function ($) {
    var $start = $('#start-date-time-datepicker'),
        $end1 = $('#end-date-time-datepicker');

    let locale = $('#pageLanguage').val();

    $start.datetimepicker({
        format: 'DD/MM/YYYY',
        locale: locale,
        minDate: moment()
    });
    // $.datepicker.regional['ru'];
    $end1.datetimepicker({
        minDate: $start.on("change.datetimepicker", function (e) {
            $end1.datetimepicker('minDate', e.date);
        }),
        locale: locale,
        // minDate: $start.data("DateTimePicker").date(),
        format: 'DD/MM/YYYY'
    });

    $start.on('dp.change', function () {

        // $start input change event
        var startDate = $(this).data("DateTimePicker").date();

        // set $end1 to $start date and set the minDate
        $end1.data("DateTimePicker").options({
            date: startDate,
            minDate: startDate
        });
    });
});