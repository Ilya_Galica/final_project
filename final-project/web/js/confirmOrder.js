jQuery(function ($) {
    $('#confirmOrder').click(function () {
        $.ajax({
            method: "POST",
            url: "/controller",
            cache: false,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
            data: {
                ajaxCommand: "confirmOrder",
                // name: $('#nameInputOrder').val(),
                // surname: $('#surnameInputOrder').val(),
                // patronymic: $('#patronymicInputOrder').val(),
                // email: $('#emailInputOrder').val(),
                // telephone: $('#telephoneInputOrder').val(),
                // jobPlace: $('#jobInputOrder').val(),
            },
            success: function (responseText) {
                alert(responseText);
                window.location.replace("/index.jsp");
            },
            error: function (request) {
                alert(request+" Что-то тут не так")
            }
        });
    });
});