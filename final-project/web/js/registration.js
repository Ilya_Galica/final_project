const FOCUS = 'focus';
const BLUR = 'blur';
const CLICK = 'click';
const REGISTRATION = '.registration-visability';
const LOGIN = '.login-visability';
const SWIPE = '.swipe';
const telephone = "#telephoneInput";

jQuery(document).ready(function ($) {
    $(function () {
        function maskPhone() {
            var country = $('#country option:selected').val();
            switch (country) {
                case "ru":
                    $(telephone).mask("+7(999)999-99-99").focus();
                    break;
                case "ua":
                    $(telephone).mask("+380(999)999-99-99").focus();
                    break;
                case "by":
                    $(telephone).mask("+375(99)999-99-99").focus();
                    break;
            }
        }

        maskPhone();
        $('#country').change(function () {
            maskPhone();

        });
    });
});


jQuery(document).ready(function ($) {


});


// jQuery(function ($) {

// });

// var ajaxSend = $.ajax({
//     method: "POST",
//     url: 'controller',
//     data: {
//         ajaxCommand: "signUp"
//     },
//     success: function (responseText) {
//         alert("rrr " + responseText + " suck")
//         // $('#ajaxGetUserServletResponse').text(responseText);
//     },
//     error: function (request) {
//         alert(request)
//     }
// });


// jQuery(document).ready(function () {
//     'use strict';
//     window.addEventListener('load', function () {
//         // Fetch all the forms we want to apply custom Bootstrap validation styles to
//         var forms = document.getElementsByClassName('needs-validation');
//         // Loop over them and prevent submission
//         var count = 0;
//         var validation = Array.prototype.filter.call(forms, function (form) {
//             form.addEventListener('submit', function (event) {
//                 if (form.checkValidity() === false) {
//                     event.preventDefault();
//                     event.stopPropagation();
//                 }
//                 form.classList.add('was-validated');
//             }, false);
//         });
//             $('input#signUp').click(function () {
//                 $.ajax({
//                     method: "POST",
//                     url: 'controller',
//                     data: {
//                         ajaxCommand: "signUp"
//                     },
//                     success: function (responseText) {
//                         alert("rrr " + responseText + " suck")
//                         // $('#ajaxGetUserServletResponse').text(responseText);
//                     },
//                     error: function (request) {
//                         alert(request)
//                     }
//                 });
//             });
//     }, false);
// });


jQuery(function ($) {
    $(".validation-data input").on(FOCUS, function () {
        $(this).addClass(FOCUS);
    })
});

jQuery(function ($) {
    $(".validation-data input").on(BLUR, function () {
        if ($(this).val() === "") {
            $(this).removeClass(FOCUS);
        }
    })
});

jQuery(function ($) {
    checkFunction(SWIPE, LOGIN, REGISTRATION);
});

var checkFunction = function (swipeClassName, hideClassName, registrateClassName) {
    $(swipeClassName).on(CLICK, function () {
        $(hideClassName).toggle();
        $(registrateClassName).toggle();
    })
};

jQuery(function ($) {
    $('#registration').on('submit', function () {
        var pass = $('.registration-visability .validation-data input[name="password"]').val();
        var confPass = $('.registration-visability .validation-data input[name="confirmPassword"]').val();
        if (pass !== confPass) {
            $('')
        } else {
            alert('pass');
        }
    });
});


// jQuery(function ($) {
//     $('#sendData').submit(function () {
//         $.ajax({
//             type: "POST",
//             url: "controller",
//             data: {ajaxCommand: "singUp"},
//             success: function (message) {
//                 alert(message)
//             },
//             error: function (XMLHttpRequest, textStatus, errorThrown) {
//                 alert("Status: " + textStatus);
//                 alert("Error: " + errorThrown);
//             }
//         })
//     });
// });


jQuery(function ($) {
    $('#addEmail').on(CLICK, function () {
        var parent = $('.extra-email');
        var validationData = $('<div class=\"validation-data extra\"></div>');
        var inputEmail = $('<input type=\"email\" name=\"extraEmail\"'
            + 'pattern=\"^[\w]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$\" required'
            + ' min=\"6\" max=\"64\">');
        var spanPtaceholder = $('<span data-palceholder=\"Extra email\"></span>');
        var spanExplain = $('<span class=\"input-explain\">example@mail.com</span>');
        var removeBtn = $('<input type=\"button\" class=\"removeEmail\" value=\"-\">');
        removeBtn.click(function () {
            $(this).parent().remove();
        });
        validationData.append(inputEmail);
        validationData.append(spanPtaceholder);
        validationData.append(spanExplain);
        validationData.append(removeBtn);
        // validationData.appendTo(parent);
        parent.append(validationData);
    });
    // $('#addEmail').on(CLICK,function(){
    //     validData.append(' <div class="validation-data extra">'
    //     +'<input type="email" name="email"'
    //     +' pattern="^[\w]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required min="6"'
    //     +' max="64"> <span data-palceholder="Email"></span>'
    //     +' <span class="input-explain">example@mail.com</span>'
    //     +' <input type="button" id="addEmail" value="+">'
    //     );
    // });
});
