// jQuery(function ($) {
//
//     window.addEventListener('load', function () {
//
//         let createOrderButton = document.getElementById('createOrder');
//
//         let createOrderForm = document.getElementById('orderForm');
//
//         if (createOrderForm != null) {
//
//             createOrderButton.addEventListener('click', function (index) {
//                 createOrderForm.classList.add('was-validated');
//                 console.log(createOrderForm.checkValidity());
//                 if (createOrderForm.checkValidity() === true) {
//                     console.log("pre button");
//                     $.ajax({
//                         method: "POST",
//                         url: 'controller',
//                         cache: false,
//                         contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
//                         data: {
//                             ajaxCommand: "createOrder",
//                             name: $('#nameInputOrder').val(),
//                             surname: $('#surnameInputOrder').val(),
//                             patronymic: $('#patronymicInputOrder').val(),
//                             email: $('#emailInputOrder').val(),
//                             telephone: $('#telephoneInputOrder').val(),
//                             jobPlace: $('#jobInputOrder').val(),
//                         },
//                         success: function (responseText) {
//                             alert(responseText);
//                             window.location.replace("/jsp/order/confirmOrderPage.jsp");
//
//                         },
//                         error: function (request) {
//                             alert(request)
//                         }
//                     });
//                 }
//             });
//         }
//     }, false);
// });


jQuery(function ($) {
    $(function () {
        function maskPhone() {
            let country = $('#countryOrder option:selected').val();
            let telephone = $('#telephoneInputOrder');
            switch (country) {
                case "ru":
                    telephone.mask("+7(999)999-99-99").focus();
                    break;
                case "ua":
                    telephone.mask("+380(999)999-99-99").focus();
                    break;
                case "by":
                    telephone.mask("+375(99)999-99-99").focus();
                    break;
            }
        }

        maskPhone();
        $('#countryOrder').change(function () {
            maskPhone();

        });
    });
});