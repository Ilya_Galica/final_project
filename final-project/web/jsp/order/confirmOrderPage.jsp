<%--@elvariable id="order" type="dao.entity.impl.Order"--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="header">
    <html lang="${sessionScope.language}" dir="ltr">
    <head>
        <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>
            <%--@declare id="exitform"--%>
            <%--@declare id="signInForm"--%>
            <%--@declare id="signupform"--%>
            <%--@declare id="changelanguage"--%>

            <%--<jsp:useBean id="hotelroom" scope="session" type="dao.entity.impl.Hotelroom"/>--%>
        <jsp:useBean id="dataContainer" scope="session" type="dao.entity.HotelSerchDataContainer"/>
        <c:set var="totalPrice" value="${requestScope.dayAmount * order.hotelroom.price}"/>
        <c:set var="discount" value="${order.discount.discountValueForJobPlace}"/>

        <title>Подтверждение заказа</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="https://kit.fontawesome.com/571b3e539c.js"></script>

        <script src="<c:url value="/js/confirmOrder.js"/>"></script>

        <link href="<c:url value="/css/contentShowRequestHotels.css"/>" rel="stylesheet">

        <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

        <link href="<c:url value="/css/footer.css"/>" rel="stylesheet">

        <script src="<c:url value="/js/bootstrapValidator.js"/>"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <c:set value="jsp/order/confirmOrderPage" var="pageURL"/>
    </head>
    <body class="m-0 p-0">
    <input type="hidden" form="exitForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="signInForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="signUpForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="changeLanguage" name="pageName" value="${pageURL}">

    <jsp:include page="/WEB-INF/page.part/header.jsp"/>


    <div class="card mt-3 mr-0">


        <div class="row justify-content-center mt-3 mr-0">
            <div class="card mb-3 col-lg-8 col-md-8 col-sm-11">
                <div class="card-body">
                    <h4 class="card-title"><fmt:message key="your.check.order"/></h4>
                    <form id="confirmOrderForm">


                        <p id="userNameP">${order.user.surname} ${order.user.name} ${order.user.patronymic},</p>
                        <p><fmt:message key="your.hotel"/> ${order.hotelroom.hotel.name}</p>
                        <p><fmt:message key="your.room.number"/> ${order.hotelroom.number}</p>
                        <p><fmt:message key="your.arrival"/> ${dataContainer.arrivalData}
                            <fmt:message key="your.departure"/> ${dataContainer.departureData}</p>
                        <p><fmt:message key="your.discount"/> <b>${order.discount.discountValueForJobPlace}%</b></p>
                        <p><fmt:message key="your.order.cost"/> <b>${order.totalPrice}$</b></p>
                        <p><fmt:message key="your.confirm"/> <b>${order.user.email}</b></p>
                    </form>
                    <div class="justify-content-center mt-3 row">
                        <form action="<c:url value="/controller"/>" method="post">
                            <input type="hidden" name="command" value="denyOrder">
                            <input class="submit-btn mr-4" id="deleteOrder" type="submit"
                                   value="<fmt:message key="your.deny.button"/>"
                            >
                        </form>
                            <%--data-toggle="modal" data-target="#createOrderModal">--%>
                        <input class="submit-btn" id="confirmOrder" type="button"
                               value="<fmt:message key="your.confirm.button"/>"
                               form="confirmOrderForm">
                            <%--data-toggle="modal" data-target="#createOrderModal">--%>
                    </div>
                </div>
            </div>
        </div>


    </div>
</fmt:bundle>
<jsp:include page="/WEB-INF/page.part/footer.jsp"/>
</body>
<%--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"--%>
<%--integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"--%>
<%--crossorigin="anonymous"></script>--%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</html>