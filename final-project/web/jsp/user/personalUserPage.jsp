<%--@elvariable id="code" type="java.lang.String"--%>
<%--@elvariable id="user" type="dao.entity.impl.User"--%>
<%--<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="personalPage">
    <html lang="${sessionScope.language}" dir="ltr">
    <head>

        <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>

        <title>Ваша личная страница</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="https://kit.fontawesome.com/571b3e539c.js"></script>

        <link href="<c:url value="/css/contentShowRequestHotels.css"/>" rel="stylesheet">

        <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

        <script src="<c:url value="/js/changePersonalParams.js"/>"></script>

        <link href="<c:url value="/css/footer.css"/>" rel="stylesheet">

        <script src="<c:url value="/js/bootstrapValidator.js"/>"></script>

        <script src="<c:url value="/js/library/jquery.maskedinput.min.js"/>"></script>


        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
    </head>
    <c:set value="/index" var="pageURL"/>
    <body class="m-0 p-0">
    <jsp:include page="/WEB-INF/page.part/header.jsp"/>

    <%--@declare id="exitform"--%>
    <%--@declare id="signInForm"--%>
    <%--@declare id="signupform"--%>
    <%--@declare id="changelanguage"--%>
    <input type="hidden" form="exitForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="signInForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="signUpForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="changeLanguage" name="pageName" value="/jsp/user/personalUserPage">
    <div class="row justify-content-center m-0 p-0">
        <div class="col-lg-5 col-md-5 col-sm-11 mt-3">
            <div class="login-form">

                <form action="controller" method="post" class="needs-validation" id="changeForm" novalidate>
                    <input type="hidden" name="command" value="changeData">
                    <div class="row">
                        <div class="form-group col-9">
                            <label for="yourEmail"><fmt:message key="change.email"/></label>

                            <input type="email" class="form-control" id="yourEmail" placeholder="Email"
                                   name="email" value="${user.email}">
                            <div class="valid-feedback">
                                <fmt:message key="right.email"/>
                            </div>
                            <div class="invalid-feedback">
                                <fmt:message key="not.right.email"/>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary col-2 align-self-center mt-3" id="changeEmail"
                               value="<fmt:message key="change"/>">
                    </div>
                </form>
                <form action="controller" method="post" class="needs-validation" novalidate>
                    <input type="hidden" name="command" value="changeData">
                    <div class="row">
                        <div class="form-group col-9">
                            <label for="yourPassword"> <fmt:message key="can.change.password"/></label>
                            <input type="text" class="form-control" id="yourPassword"
                                   placeholder=" <fmt:message key="password"/>"
                                   value="${user.password}"
                                   name="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(\w){6,30}">
                            <div class="valid-feedback">
                                <fmt:message key="right.password"/>
                            </div>
                            <div class="invalid-feedback">
                                <fmt:message key="not.right.password"/>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary col-2 align-self-center mt-3" id="changePass"
                               value="<fmt:message key="change"/>">
                    </div>
                </form>
                <form action="controller" method="post" class="needs-validation" novalidate>
                    <input type="hidden" name="command" value="changeData">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="yourCountry"><fmt:message key="country"/></label>
                            <select id="yourCountry" class="form-control">
                                <option value="ua"><fmt:message key="ukraine"/> +380</option>
                                <option value="by" selected><fmt:message key="belarus"/> +375</option>
                            </select>
                        </div>
                        <div class=" col-md-6">
                            <label for="yourTelephone"> <fmt:message key="can.change.telephone"/></label>
                            <input type="text" class="form-control" id="yourTelephone"
                                   placeholder=" <fmt:message key="telephone"/>"
                                   name="telephone" value="${user.telephone}"
                                   pattern="((80)?|(\+375)?)(\(\d{2}\)|\d{2})((\d{3}-\d{2}-\d{2})|\d{7})">
                            <div class="valid-feedback">
                                <fmt:message key="right.telephone"/>
                            </div>
                            <div class="invalid-feedback">
                                <fmt:message key="not.right.telephone"/>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary col-2 align-self-center mt-3" id="changeTel"
                               value="<fmt:message key="change"/>">
                    </div>
                </form>
                <form action="controller" method="post" class="needs-validation" novalidate>
                    <input type="hidden" name="command" value="changeData">
                    <div class="row align-content-center">
                            <%--<form id="changeJobForm" class="needs-validation" novalidate>--%>
                        <div class="form-group col-9">
                            <label for="yourWork"><fmt:message key="can.change.job"/></label>
                            <input type="text" class="form-control" id="yourWork"
                                   placeholder=" <fmt:message key="job"/>"
                                   name="job" aria-describedby="yourWork" value="${sessionScope.user.jobPlace}">
                            <small id="jobInputHelp" class="text-muted">
                                <fmt:message key="job.opportunity"/>
                            </small>
                        </div>
                        <input type="submit" class="btn btn-primary col-2 align-self-center mb-3" id="changeJob"
                               value="<fmt:message key="change"/>">
                            <%--</form>--%>
                    </div>
                </form>
            </div>
        </div>

    </div>
</fmt:bundle>

<jsp:include page="/WEB-INF/page.part/footer.jsp"/>
</body>
<%--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"--%>
<%--integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"--%>
<%--crossorigin="anonymous"></script>--%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</html>
