<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>

    <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="<c:url value="/js/bootstrapValidator.js"/>"></script>

    <script src="https://kit.fontawesome.com/571b3e539c.js"></script>

</head>
<body>
<%--@elvariable id="user" type=""--%>
<c:set var="pageURL" value="/index"/>

<header class="sticky-top navbar navbar-expand-lg navbar-light bg-primary col-lg-12 col-md-12 col-sm-12 m-0 p-0">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerNavbar"
            aria-controls="headerNavbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse row justify-content-around m-0" id="headerNavbar">
        <a href="<c:url value="/index.jsp"/>" class="mt-1">
            <h2 class="logo navbar-brand text-white">
                <i class="fas fa-umbrella-beach"></i>OneDirection</h2>
        </a>
        <div class="row">
            <div class="dropdown p-0">
                <button class="nav-item text-white dropdown-toggle p-0 ml-2" id="autButton"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">${user.name}</button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <form id="exitForm" method="post" action="controller">
                        <input type="hidden" name="command" value="profileExit">
                        <input type="hidden" form="exitForm" name="pageName" value="${pageURL}">
                        <input class="dropdown-item" type="submit" id="profileExit" form="exitForm"
                               value="Выход">
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="accordion" id="adminAction">
    <div class="card">
        <div class="card-header" id="headingOne">
            <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                        aria-expanded="true" aria-controls="collapseOne">
                    Просмотреть информацию
                </button>
            </h2>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#adminAction">
            <div class="card-body">

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="findAllUserAdmin">
                    <input type="submit" class="btn btn-primary" value="Запросить пользователей">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#showAllUserModal">
                        Пользователи
                    </button>
                    <!-- User Modal -->

                    <div class="modal fade" id="showAllUserModal" tabindex="-1" role="dialog"
                         aria-labelledby="showAllUserModalTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="showAllUserModalTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Id</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Имя</th>
                                            <th scope="col">Фамилия</th>
                                            <th scope="col">Отчество</th>
                                            <th scope="col">Телефон</th>
                                            <th scope="col">Работа</th>
                                            <th scope="col">Роль</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:set var="countUser" value="0" scope="page"/>
                                        <c:forEach items="${sessionScope.users}" var="user">
                                            <c:set var="countUser" value="${countUser + 1}" scope="page"/>
                                            <tr>
                                                <th scope="row"><c:out value="${countUser+1}"/></th>
                                                <td>${user.id}</td>
                                                <td>${user.email}</td>
                                                <td>${user.name}</td>
                                                <td>${user.surname}</td>
                                                <td>${user.patronymic}</td>
                                                <td>${user.telephone}</td>
                                                <td>${user.jobPlace}</td>
                                                <td>${user.role}</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="findAllDiscountAdmin">
                    <input type="submit" class="btn btn-primary" value="Запросить скидки">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#showAllDiscountModal">
                        Скидки
                    </button>

                    <!-- User Modal -->

                    <div class="modal fade" id="showAllDiscountModal" tabindex="-1" role="dialog"
                         aria-labelledby="showAllDiscountModalTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="showAllDiscountModalTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Id</th>
                                            <th scope="col">Работа пользователя</th>
                                            <th scope="col">Скидка</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:set var="countDiscount" value="0" scope="page"/>
                                        <c:forEach items="${sessionScope.discounts}" var="discount">
                                            <c:set var="countDiscount" value="${countDiscount + 1}" scope="page"/>
                                            <tr>
                                                <th scope="row"><c:out value="${countDiscount}"/></th>
                                                <td>${discount.id}</td>
                                                <td>${discount.userJobPlace}</td>
                                                <td>${discount.discountValueForJobPlace}</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="findAllOrderAdmin">
                    <input type="submit" class="btn btn-primary" value="Запросить заказы">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#showAllOrderModal">
                        Заказы
                    </button>

                    <!-- User Modal -->

                    <div class="modal fade" id="showAllOrderModal" tabindex="-1" role="dialog"
                         aria-labelledby="showAllOrderModalTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="showAllOrderModalTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Id</th>
                                            <th scope="col">Id скидки</th>
                                            <th scope="col">Id заказчика</th>
                                            <th scope="col">Дата прибытия</th>
                                            <th scope="col">Дата выезда</th>
                                            <th scope="col">Id комнаты</th>
                                            <th scope="col">ИТОГО</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:set var="countOrder" value="0" scope="page"/>
                                        <c:forEach items="${sessionScope.orders}" var="order">
                                            <c:set var="countOrder" value="${countOrder + 1}" scope="page"/>
                                            <tr>
                                                <th scope="row"><c:out value="${countOrder}"/></th>
                                                <td>${order.id}</td>
                                                <td>${order.discount}</td>
                                                <td>${order.user}</td>
                                                <td>${order.arrivalData}</td>
                                                <td>${order.departureData}</td>
                                                <td>${order.hotelroom}</td>
                                                <td>${order.totalPrice}</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="findAllHotelAdmin">
                    <input type="submit" class="btn btn-primary" value="Запросить отели">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#showAllHotelModal">
                        Заказы
                    </button>

                    <!-- User Modal -->

                    <div class="modal fade" id="showAllHotelModal" tabindex="-1" role="dialog"
                         aria-labelledby="showAllHotelModalTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="showAllHotelModalTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Id</th>
                                            <th scope="col">Название</th>
                                            <th scope="col">Город</th>
                                            <th scope="col">Страна</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:set var="countHotel" value="0" scope="page"/>
                                        <c:forEach items="${sessionScope.hotels}" var="hotel">
                                            <c:set var="countHotel" value="${countHotel + 1}" scope="page"/>
                                            <tr>
                                                <th scope="row"><c:out value="${countHotel}"/></th>
                                                <td>${hotel.id}</td>
                                                <td>${hotel.name}</td>
                                                <td>${hotel.city}</td>
                                                <td>${hotel.country}</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

                <form action="<c:url value="/controller"/>" method="post">

                    <input type="hidden" name="command" value="findAllHotelroomAdmin">
                    <input type="submit" class="btn btn-primary" value="Запросить комнаты">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#showAllHotelroomModal">
                        Заказы
                    </button>


                    <div class="modal fade" id="showAllHotelroomModal" tabindex="-1" role="dialog"
                         aria-labelledby="showAllHotelroomModalTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="showAllHotelroomModalTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Id</th>
                                            <th scope="col">Номер комнаты</th>
                                            <th scope="col">Id отеля</th>
                                            <th scope="col">Цена</th>
                                            <th scope="col">Вместительность номера</th>
                                            <th scope="col">Уровень комфорта</th>
                                            <th scope="col">Описание</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:set var="countHotelroom" value="0" scope="page"/>
                                        <c:forEach items="${sessionScope.holetroomList}" var="hotelroom">
                                            <c:set var="countHotelroom" value="${countHotelroom + 1}" scope="page"/>
                                            <tr>
                                                <th scope="row"><c:out value="${countHotelroom}"/></th>
                                                <td>${hotelroom.id}</td>
                                                <td>${hotelroom.number}</td>
                                                <td>${hotelroom.hotel}</td>
                                                <td>${hotelroom.price}</td>
                                                <td>${hotelroom.peopleCapacity}</td>
                                                <td>${hotelroom.comfort}</td>
                                                <td>${hotelroom.imageURL}</td>
                                                <td>${hotelroom.description}</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

    <%-----------------------------------------------FIND_BY_ID-----------------------------------------%>

    <div class="card">
        <div class="card-header" id="headingTwo">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo"
                        aria-expanded="false" aria-controls="collapseTwo">
                    Просмотреть выборочно информацию
                </button>
            </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#adminAction">
            <div class="card-body">
                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="findUserAdmin">
                    <input type="submit" class="btn btn-primary" value="Найти юзера">
                    <input type="text" name="findId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.findUser!=null}">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Id</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Имя</th>
                                    <th scope="col">Фамилия</th>
                                    <th scope="col">Отчество</th>
                                    <th scope="col">Телефон</th>
                                    <th scope="col">Работа</th>
                                    <th scope="col">Роль</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:set var="findUser" scope="session" value="${findUser}"/>
                                <tr>
                                    <th scope="row">${countUser+1}</th>
                                    <td>${findUser.id}</td>
                                    <td>${findUser.email}</td>
                                    <td>${findUser.name}</td>
                                    <td>${findUser.surname}</td>
                                    <td>${findUser.patronymic}</td>
                                    <td>${findUser.telephone}</td>
                                    <td>${findUser.jobPlace}</td>
                                    <td>${findUser.role}</td>
                                </tr>
                                </tbody>
                            </table>
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>
                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="findDiscountAdmin">
                    <input type="submit" class="btn btn-primary" value="Найти скидку">
                    <input type="text" name="findId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.findDiscount!=null && sessionScope.findDiscount.id!=null}">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Работа пользователя</th>
                                    <th scope="col">Скидка</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:set var="findDiscount" scope="session" value="${findDiscount}"/>
                                <tr>
                                    <td>${findDiscount.id}</td>
                                    <td>${findDiscount.userJobPlace}</td>
                                    <td>${findDiscount.discountValueForJobPlace}</td>
                                </tr>
                                </tbody>
                            </table>
                        </c:when>
                        <c:when test="${sessionScope.findDiscount!=null && findDiscount.id==null}">
                            ${findDiscount}
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="findHotelAdmin">
                    <input type="submit" class="btn btn-primary" value="Найти отель">
                    <input type="text" name="findId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.findHotel!=null && sessionScope.findHotel.id!=null}">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Название</th>
                                    <th scope="col">Город</th>
                                    <th scope="col">Страна</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:set var="findHotel" scope="session" value="${findHotel}"/>
                                <tr>
                                    <td>${findHotel.id}</td>
                                    <td>${findHotel.name}</td>
                                    <td>${findHotel.city}</td>
                                    <td>${findHotel.country}</td>
                                </tr>
                                </tbody>
                            </table>
                        </c:when>
                        <c:when test="${sessionScope.findHotel!=null && findHotel.id==null}">
                            ${findHotel}
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="findOrderAdmin">
                    <input type="submit" class="btn btn-primary" value="Найти заказ">
                    <input type="text" name="findId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.findOrder!=null && sessionScope.findOrder.id!=null}">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Id скидки</th>
                                    <th scope="col">Id заказчика</th>
                                    <th scope="col">Дата прибытия</th>
                                    <th scope="col">Дата выезда</th>
                                    <th scope="col">Id комнаты</th>
                                    <th scope="col">ИТОГО</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:set var="findOrder" scope="session" value="${findOrder}"/>
                                <tr>
                                    <td>${findOrder.id}</td>
                                    <td>${findOrder.discount}</td>
                                    <td>${findOrder.user}</td>
                                    <td>${findOrder.arrivalData}</td>
                                    <td>${findOrder.departureData}</td>
                                    <td>${findOrder.hotelroom}</td>
                                    <td>${findOrder.totalPrice}</td>
                                </tr>
                                </tbody>
                            </table>
                        </c:when>
                        <c:when test="${sessionScope.findOrder!=null && findOrder.id==null}">
                            ${findOrder}
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="findHotelroomAdmin">
                    <input type="submit" class="btn btn-primary" value="Найти комнату">
                    <input type="text" name="findId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.findHotelroom!=null && sessionScope.findHotelroom.id!=null}">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Номер комнаты</th>
                                    <th scope="col">Id отеля</th>
                                    <th scope="col">Цена</th>
                                    <th scope="col">Вместительность номера</th>
                                    <th scope="col">Уровень комфорта</th>
                                    <th scope="col">Описание</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:set var="findHotelroom" scope="session" value="${findHotelroom}"/>
                                <tr>
                                    <td>${findHotelroom.id}</td>
                                    <td>${findHotelroom.number}</td>
                                    <td>${findHotelroom.hotel}</td>
                                    <td>${findHotelroom.price}</td>
                                    <td>${findHotelroom.peopleCapacity}</td>
                                    <td>${findHotelroom.comfort}</td>
                                    <td>${findHotelroom.imageURL}</td>
                                    <td>${findHotelroom.description}</td>
                                </tr>
                                </tbody>
                            </table>
                        </c:when>
                        <c:when test="${sessionScope.findHotelroom!=null && findHotelroom.id==null}">
                            ${findHotelroom}
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>
            </div>
        </div>
    </div>

    <%--                                                               <DELETE>              --%>

    <div class="card">
        <div class="card-header" id="headingThree">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree"
                        aria-expanded="false" aria-controls="collapseThree">
                    Удалить информацию
                </button>
            </h2>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#adminAction">
            <div class="card-body">
                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="deleteUserAdmin">
                    <input type="submit" class="btn btn-primary" value="Долгий бан">
                    <input type="text" name="deleteId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.deleteUser!=null}">
                            ${sessionScope.deleteUser}
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>
                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="deleteDiscountAdmin">
                    <input type="submit" class="btn btn-primary" value="Удалить скидку">
                    <input type="text" name="deleteId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.deleteDiscount!=null}">
                            ${sessionScope.deleteDiscount}
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="deleteHotelroomAdmin">
                    <input type="submit" class="btn btn-primary" value="Удалить комнату">
                    <input type="text" name="deleteId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.deleteHotelroom!=null}">
                            ${sessionScope.deleteHotelroom}
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="deleteOrderAdmin">
                    <input type="submit" class="btn btn-primary" value="Удалить заказ">
                    <input type="text" name="deleteId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.deleteOrder!=null}">
                            ${sessionScope.deleteOrder}
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>

                <form action="<c:url value="/controller"/>" method="post">
                    <input type="hidden" name="command" value="deleteHotelAdmin">
                    <input type="submit" class="btn btn-primary" value="Удалить отель">
                    <input type="text" name="deleteId" required>
                    <!-- User Modal -->

                    <c:choose>
                        <c:when test="${sessionScope.deleteHotel!=null}">
                            ${sessionScope.deleteHotel}
                        </c:when>
                        <c:otherwise>
                            Тут будет результат или не будет
                        </c:otherwise>
                    </c:choose>

                </form>

            </div>
        </div>
    </div>
<%--                                                     CREATE                                             --%>
    <div class="card">
        <div class="card-header" id="headingFour">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour"
                        aria-expanded="false" aria-controls="collapseFour">
                    Добавить информацию
                </button>
            </h2>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#adminAction">
            <div class="card-body col-12">
                <div class="col-12">
                    <form action="<c:url value="/controller"/>" method="post" class="col-12 row">
                        <input type="hidden" name="command" value="createDiscountAdmin">


                        <input type="text" class="form-control col-3" placeholder="Работа дающая скидку" name="insertUserJobPlace" required
                        pattern="[a-zA-Zа-яА-Я\s]{2,}">
                        <input type="text" class="form-control col-3" placeholder="Скидка" name="insertDiscountValueForJobPlace" required
                        pattern="[0-9]{1,2}">
                        <input type="submit" class="btn btn-primary col-3" value="Добавить скидку">
                        <!-- User Modal -->

                        <c:choose>
                            <c:when test="${sessionScope.createDiscount!=null}">
                               <div class="col-12  justify-self-center"> ${sessionScope.createDiscount}</div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-12 justify-self-center">  Тут будет результат или не будет</div>
                            </c:otherwise>
                        </c:choose>

                    </form>
                </div>
                <div class="col-12">
                    <form action="<c:url value="/controller"/>" method="post" class="col-12 row">
                        <input type="hidden" name="command" value="createHotelroomAdmin">


                        <input type="text" class="col-3 form-control" placeholder="Номер комнаты" name="insertNumber" required
                        pattern="[0-9]{1,3}">
                        <input type="text" class="col-3 form-control" placeholder="id отеля" name="insertHotelId" required
                        pattern="[0-9]{1,2}">
                        <input type="text" class="col-3 form-control" placeholder="Цена за ночь" name="insertPrice" required
                        pattern="[0-9]{1,4}">
                        <input type="text" class="col-3 form-control" placeholder="Вместительность (до 5)" name="insertCapacity"
                               required
                        pattern="[0-5]{1}">

                        <select name="insertComfort" class="input-data col-3 form-control" id="insertComfort">
                            <option>lux</option>
                            <option>halflux</option>
                            <option>common</option>
                        </select>
                        <input type="text" class="col-3 form-control" placeholder="Адрес картинки" name="insertImgURL" required>
                        <input class="col-4" type="text" placeholder="Описание камнаты и отеля" name="insertDescription"
                        required>
                        <input class="col-4 form-control" type="text" placeholder="Описание камнаты и отеля на английском"
                        name="insertDescriptionEn" required>

                        <input type="submit" class="btn btn-primary col-3" value="Добавить номер">
                        <!-- User Modal -->

                        <c:choose>
                            <c:when test="${sessionScope.createHotelroom!=null}">
                                <div class="col-12  justify-self-center">${sessionScope.createHotelroom}</div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-12  justify-self-center"> Тут будет результат или не будет</div>
                            </c:otherwise>
                        </c:choose>

                    </form>
                </div>
                <div class="col-12">
                    <form action="<c:url value="/controller"/>" method="post" class="col-12 row">
                        <input type="hidden" name="command" value="createHotelAdmin">


                        <input type="text" class="col-3 form-control" placeholder="Название отеля" name="insertHotelName" required
                        pattern="[a-zA-Zа-ЯА-Я\s]{1,}">
                        <input type="text" class="col-3 form-control" placeholder="Название города" name="insertCity" required
                        pattern="[a-zA-Z\s]{1,}">
                        <input type="text" class="col-3 form-control" placeholder="Название страны" name="insertCountry" required
                        pattern="[a-zA-Z\s]{1,}">

                        <input type="submit" class="btn btn-primary col-3" value="Добавить oтель">
                        <!-- User Modal -->

                        <c:choose>
                            <c:when test="${sessionScope.createHotel!=null}">
                                <div class="col-12  justify-self-center">${sessionScope.createHotel}</div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-12 justify-self-center"> Тут будет результат или не будет</div>
                            </c:otherwise>
                        </c:choose>

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</html>
