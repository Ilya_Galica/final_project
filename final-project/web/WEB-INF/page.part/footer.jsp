<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<%--<link href="/css/content.css" rel="stylesheet">--%>
<footer class="row justify-content-center m-0">
    <a class="social-media" href="#">
        <i class="fab fa-vk"></i>
    </a>
    <a class="social-media" href="#">
        <i class="fab fa-facebook-f"></i>
    </a>
    <a class="social-media" href="#">
        <i class="fab fa-google"></i>
    </a>
    <a class="social-media" href="#">
        <i class="fab fa-instagram"></i>
    </a>
    <a class="social-media" href="#">
        <i class="fab fa-youtube"></i>
    </a>
</footer>