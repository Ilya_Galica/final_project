<%--suppress XmlDuplicatedId --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--<script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>--%>
<link href="<c:url value="/css/registrate.css"/>" rel="stylesheet">
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<script src="<c:url value="/js/registration.js"/>"></script>

<script src="<c:url value="/js/library/jquery.maskedinput.min.js"/>"></script>

<script src="<c:url value="/js/bootstrapValidator.js"/>"></script>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="header">
    <%--crossorigin="anonymous"></script>--%>
    <div class="modal fade" id="registrate" tabindex="-1" role="dialog" aria-labelledby="createOrderModalTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createOrderModalTitle"><fmt:message key="authorisation"/></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="login-visability">

                        <form action="controller" method="post" class="needs-validation" id="signInForm">
                            <input type="hidden" name="command" value="signIn">
                            <h3><fmt:message key="enter"/></h3>
                            <div class="form-group">
                                <label for="emailInputLogin">Email</label>
                                <input type="email" class="form-control" id="emailInputLogin"
                                       placeholder="Email"
                                       name="loginEmail" required>
                                <div class="invalid-feedback">
                                    <fmt:message key="enter.email"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="passwordInputLogin"> <fmt:message key="password"/></label>
                                <input type="password" class="form-control" id="passwordInputLogin"
                                       placeholder="<fmt:message key="password"/>"
                                       name="password" required>
                                <div class="invalid-feedback">
                                    <fmt:message key="enter.password"/>
                                </div>
                            </div>


                            <div class="bottom-text">
                                <fmt:message key="not.have.account"/>? <a class="swipe" href="#">
                                <fmt:message key="registrate"/></a>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    <fmt:message key="close"/>
                                </button>
                                <input type="submit" class="btn btn-primary" form="signInForm" id="signIn"
                                       value="<fmt:message key="enter"/>">
                            </div>
                        </form>
                    </div>

                    <div class="registration-visability login-form">

                        <form action="controller" method="post" class="needs-validation" id="signUpForm"
                              novalidate>
                            <input type="hidden" name="command" value="signUp">
                                <%--<input type="hidden" name="ajaxCommand" value="signUp">--%>
                            <h3><fmt:message key="registration"/></h3>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="nameInput"><fmt:message key="name"/></label>
                                    <input type="text" class="form-control" id="nameInput"
                                           placeholder="<fmt:message key="name"/>"
                                           name="name" pattern="[a-zA-Zа-яА-Я]{2,}" required>
                                    <div class="valid-feedback">
                                        <fmt:message key="right.name"/>
                                    </div>
                                    <div class="invalid-feedback">
                                        <fmt:message key="not.right.name"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="surnameInput"><fmt:message key="surname"/></label>
                                    <input type="text" class="form-control" id="surnameInput"
                                           placeholder="<fmt:message key="surname"/>"
                                           name="surname" pattern="[a-zA-Zа-яА-Я]{2,}" required>
                                    <div class="valid-feedback">
                                        <fmt:message key="right.surname"/>
                                    </div>
                                    <div class="invalid-feedback">
                                        <fmt:message key="not.right.surname"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPatronymic"><fmt:message key="patronymic"/></label>
                                <input type="text" class="form-control" id="inputPatronymic"
                                       placeholder="<fmt:message key="patronymic"/>"
                                       name="patronymic" pattern="[a-zA-Zа-яА-Я]{2,}" required>
                                <div class="valid-feedback">
                                    <fmt:message key="right.patronymic"/>
                                </div>
                                <div class="invalid-feedback">
                                    <fmt:message key="not.right.patronymic"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="emailInputRegister">Email</label>
                                <input type="email" class="form-control" id="emailInputRegister"
                                       placeholder="Email"
                                       name="email" required>
                                <div class="valid-feedback">
                                    <fmt:message key="right.email"/>
                                </div>
                                <div class="invalid-feedback">
                                    <fmt:message key="not.right.email"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="passwordInputRegister"><fmt:message key="password"/></label>
                                <input type="password" class="form-control" id="passwordInputRegister"
                                       placeholder="<fmt:message key="password"/>"
                                       name="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(\w){6,30}"
                                       required>
                                <div class="valid-feedback">
                                    <fmt:message key="right.password"/>
                                </div>
                                <div class="invalid-feedback">
                                    <fmt:message key="not.right.password"/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="country"><fmt:message key="choose.country"/></label>
                                    <select id="country" class="form-control">
                                        <option value="ru">Россия +7</option>
                                        <option value="ua">Украина +380</option>
                                        <option value="by" selected>Белорусь +375</option>
                                    </select>
                                </div>
                                <div class=" col-md-6">
                                    <label for="telephoneInput"><fmt:message key="telephone"/></label>

                                    <input type="text" class="form-control" id="telephoneInput"
                                           placeholder="<fmt:message key="telephone"/>"
                                           name="telephone"
                                           pattern="((80)?|(\+375)?)(\(\d{2}\)|\d{2})((\d{3}-\d{2}-\d{2})|\d{7})"
                                           required>
                                    <div class="valid-feedback">
                                        <fmt:message key="right.telephone"/>
                                    </div>
                                    <div class="invalid-feedback">
                                        <fmt:message key="not.right.telephone"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jobInput"><fmt:message key="job"/></label>
                                <input type="text" class="form-control" id="jobInput"
                                       placeholder="<fmt:message key="job"/>"
                                       name="jobPlace" aria-describedby="jobInputHelp">
                                <small id="jobInputHelp" class="text-muted">
                                    <fmt:message key="job.opportunity"/>
                                </small>
                            </div>
                            <div class="bottom-text">
                                <fmt:message key="have.account"/>? <a class="swipe" href="#">
                                <fmt:message key="enter"/></a>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    <fmt:message key="close"/>
                                </button>
                                <input type="submit" class="btn btn-primary" id="signUp"
                                       value="<fmt:message key="registrate"/>">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fmt:bundle>
