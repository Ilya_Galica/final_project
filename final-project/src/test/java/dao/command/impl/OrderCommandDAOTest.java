package dao.command.impl;

import dao.ConnectionPool;
import dao.entity.Entity;
import dao.entity.UserRole;
import dao.entity.impl.*;
import dao.impl.AbstractDAO;
import dao.impl.HotelroomDAOImpl;
import dao.impl.OrderDAOImpl;
import helper.ServletContextConnector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.*;

public class OrderCommandDAOTest {
    private AbstractDAO abstractDAO = OrderDAOImpl.getInstance();
    private final static Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void setUpClass() {
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testFindAll() {
        List<Entity> list = abstractDAO.findAll();
        Order actual = (Order) list.get(1);
        Order expected = new Order();
        expected.setId(2);
        User user = new User();
        user.setId(1);
        user.setEmail("fifth@mail.com");
        user.setName("fifth");
        user.setSurname("secondfifth");
        user.setPatronymic("patronomicfifth");
        user.setTelephone("3752956781234");
        user.setPassword("456123");
        user.setJobPlace("EPAM");
        user.setRole(UserRole.USER);
        expected.setUser(user);
        Discount discount = new Discount();
        discount.setId(1);
        discount.setUserJobPlace("Школа");
        discount.setDiscountValueForJobPlace(5);
        expected.setDiscount(discount);
        expected.setArrivalData("2019-03-30");
        expected.setDepartureData("2019-04-02");
        Hotelroom hotelroom = new Hotelroom();
        hotelroom.setId(10);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("Agata");
        hotel.setCity("Minsk");
        hotel.setCountry("Belarus");
        hotelroom.setHotel(hotel);
        hotelroom.setNumber(10);
        hotelroom.setPrice(25);
        hotelroom.setPeopleCapacity(2);
        hotelroom.setComfort("halflux");
        hotelroom.setImageURL("images/1.jpg");
        hotelroom.setDescription("В распоряжении гостей каждого номера гостиная зона");
        hotelroom.setDescriptionEn("At guests ' disposal in each room a Seating area");
        expected.setHotelroom(hotelroom);
        expected.setTotalPrice(0);
        assertEquals(actual, expected);
    }

    @Test
    public void testFindById() {
        Order actual = null;
        if (abstractDAO.findById(2).isPresent()) {
            actual = (Order) abstractDAO.findById(2).get();
        }
        Order expected = new Order();
        expected.setId(2);
        User user = new User();
        user.setId(1);
        user.setEmail("fifth@mail.com");
        user.setName("fifth");
        user.setSurname("secondfifth");
        user.setPatronymic("patronomicfifth");
        user.setTelephone("3752956781234");
        user.setPassword("456123");
        user.setJobPlace("EPAM");
        user.setRole(UserRole.USER);
        expected.setUser(user);
        Discount discount = new Discount();
        discount.setId(1);
        discount.setUserJobPlace("Школа");
        discount.setDiscountValueForJobPlace(5);
        expected.setDiscount(discount);
        expected.setArrivalData("2019-03-30");
        expected.setDepartureData("2019-04-02");
        Hotelroom hotelroom = new Hotelroom();
        hotelroom.setId(10);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("Agata");
        hotel.setCity("Minsk");
        hotel.setCountry("Belarus");
        hotelroom.setHotel(hotel);
        hotelroom.setNumber(10);
        hotelroom.setPrice(25);
        hotelroom.setPeopleCapacity(2);
        hotelroom.setComfort("halflux");
        hotelroom.setImageURL("images/1.jpg");
        hotelroom.setDescription("В распоряжении гостей каждого номера гостиная зона");
        hotelroom.setDescriptionEn("At guests ' disposal in each room a Seating area");
        expected.setHotelroom(hotelroom);
        expected.setTotalPrice(0);
        assertEquals(actual, expected);
    }

    @Test
    public void testDelete() {
        Order expected = new Order();
        expected.setId(1000);
        boolean actual = abstractDAO.delete(expected);
        assertFalse(actual);
    }

    @Test
    public void testCreate() {
        Order expected = new Order();
        User user = new User();
        user.setId(1);
        user.setEmail("fifth@mail.com");
        user.setName("fifth");
        user.setSurname("secondfifth");
        user.setPatronymic("patronomicfifth");
        user.setTelephone("3752956781234");
        user.setPassword("456123");
        user.setJobPlace("EPAM");
        user.setRole(UserRole.USER);
        expected.setUser(user);
        Discount discount = new Discount();
        discount.setId(1);
        discount.setUserJobPlace("Школа");
        discount.setDiscountValueForJobPlace(5);
        expected.setDiscount(discount);
        expected.setArrivalData("2020-03-30");
        expected.setDepartureData("2020-04-02");
        Hotelroom hotelroom = new Hotelroom();
        hotelroom.setId(10);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("Agata");
        hotel.setCity("Minsk");
        hotel.setCountry("Belarus");
        hotelroom.setHotel(hotel);
        hotelroom.setNumber(10);
        hotelroom.setPrice(25);
        hotelroom.setPeopleCapacity(2);
        hotelroom.setComfort("halflux");
        hotelroom.setImageURL("images/1.jpg");
        hotelroom.setDescription("В распоряжении гостей каждого номера гостиная зона");
        hotelroom.setDescriptionEn("At guests ' disposal in each room a Seating area");
        expected.setHotelroom(hotelroom);
        expected.setTotalPrice(-1);
        boolean actual = abstractDAO.create(expected);
        assertTrue(actual);
    }

    @AfterClass
    void deleteInserted() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM `order` WHERE total_price = ? ");
            statement.setInt(1, -1);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
    }
}