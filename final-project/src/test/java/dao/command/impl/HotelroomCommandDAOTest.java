package dao.command.impl;

import dao.ConnectionPool;
import dao.entity.Entity;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import dao.impl.AbstractDAO;
import dao.impl.HotelDAOImpl;
import dao.impl.HotelroomDAOImpl;
import helper.ServletContextConnector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.*;

public class HotelroomCommandDAOTest {
    private AbstractDAO abstractDAO = HotelroomDAOImpl.getInstance();
    private final static Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void setUpClass() {
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testFindAll() {
        List<Entity> list = abstractDAO.findAll();
        Hotelroom actual = (Hotelroom) list.get(1);
        Hotelroom expected = new Hotelroom();
        expected.setId(10);
        expected.setNumber(10);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("Agata");
        hotel.setCity("Minsk");
        hotel.setCountry("Belarus");
        expected.setHotel(hotel);
        expected.setPrice(25);
        expected.setPeopleCapacity(2);
        expected.setComfort("halflux");
        expected.setImageURL("images/1.jpg");
        expected.setDescription("В распоряжении гостей каждого номера гостиная зона");
        expected.setDescriptionEn("At guests ' disposal in each room a Seating area");
        assertEquals(actual, expected);
    }

    @Test
    public void testFindById() {
        Hotelroom actual = null;
        if (abstractDAO.findById(10).isPresent()) {
            actual = (Hotelroom) abstractDAO.findById(10).get();
        }
        Hotelroom expected = new Hotelroom();
        expected.setId(10);
        expected.setNumber(10);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("Agata");
        hotel.setCity("Minsk");
        hotel.setCountry("Belarus");
        expected.setHotel(hotel);
        expected.setPrice(25);
        expected.setPeopleCapacity(2);
        expected.setComfort("halflux");
        expected.setImageURL("images/1.jpg");
        expected.setDescription("В распоряжении гостей каждого номера гостиная зона");
        expected.setDescriptionEn("At guests ' disposal in each room a Seating area");
        assertEquals(actual, expected);
    }

    @Test
    public void testDelete() {
        Hotelroom expected = new Hotelroom();
        expected.setId(1000);
        boolean actual = abstractDAO.delete(expected);
        assertFalse(actual);
    }

    @Test
    public void testCreate() {
        Hotelroom expected = new Hotelroom();
        expected.setNumber(2000);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("Agata");
        hotel.setCity("Minsk");
        hotel.setCountry("Belarus");
        expected.setHotel(hotel);
        expected.setPrice(2000);
        expected.setPeopleCapacity(0);
        expected.setComfort("halflux");
        expected.setImageURL("Nothing");
        expected.setDescription("Nothing");
        expected.setDescriptionEn("Nothing");
        boolean actual = abstractDAO.create(expected);
        assertTrue(actual);
    }

    @AfterClass
    void deleteInserted() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM hotelroom WHERE capacity = ? " +
                    " and number = ? and price = ?");
            statement.setInt(1, 0);
            statement.setInt(2, 2000);
            statement.setInt(3, 2000);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
    }
}