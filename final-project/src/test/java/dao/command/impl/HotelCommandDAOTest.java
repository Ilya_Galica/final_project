package dao.command.impl;

import dao.ConnectionPool;
import dao.entity.Entity;
import dao.entity.impl.Discount;
import dao.entity.impl.Hotel;
import dao.impl.AbstractDAO;
import dao.impl.DiscountDAOImpl;
import dao.impl.HotelDAOImpl;
import helper.ServletContextConnector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.*;

public class HotelCommandDAOTest {
    private AbstractDAO abstractDAO = HotelDAOImpl.getInstance();
    private final static Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void setUpClass() {
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testFindAll() {
        List<Entity> list = abstractDAO.findAll();
        Hotel actual = (Hotel) list.get(0);
        Hotel expected = new Hotel();
        expected.setId(1);
        expected.setName("Agata");
        expected.setCity("Minsk");
        expected.setCountry("Belarus");
        assertEquals(actual, expected);
    }

    @Test
    public void testFindById() {
        Hotel actual = null;
        if (abstractDAO.findById(1).isPresent()) {
            actual = (Hotel) abstractDAO.findById(1).get();
        }
        Hotel expected = new Hotel();
        expected.setId(1);
        expected.setName("Agata");
        expected.setCity("Minsk");
        expected.setCountry("Belarus");
        assertEquals(actual, expected);
    }

    @Test
    public void testDelete() {
        Hotel hotel = new Hotel();
        hotel.setId(1000);
        boolean actual = abstractDAO.delete(hotel);
        assertFalse(actual);
    }

    @Test
    public void testCreate() {
        Hotel expected = new Hotel();
        expected.setName("Nothing");
        expected.setCity("Nothing");
        expected.setCountry("Nothing");
        boolean actual = abstractDAO.create(expected);
        assertTrue(actual);
    }

    @AfterClass
    void deleteInserted() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM hotel WHERE city= ? " +
                    " and country = ? and name = ?");
            statement.setString(1, "Nothing");
            statement.setString(2, "Nothing");
            statement.setString(3, "Nothing");
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
    }
}