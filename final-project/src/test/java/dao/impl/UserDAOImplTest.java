package dao.impl;

import dao.ConnectionPool;
import dao.UserDAO;
import dao.entity.exception.DuplicateEmailException;
import dao.entity.exception.DuplicateTelephoneException;
import dao.entity.impl.User;
import helper.ServletContextConnector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.util.CryptoUtil;
import service.util.ServiceUtil;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.testng.Assert.*;

public class UserDAOImplTest {
    private final static Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void setUpClass() {
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testSignInTrue() {
        CryptoUtil util = CryptoUtil.getInstance();
        User user = new User();
        user.setEmail("ilyagalica@gmail.com");
        try {
            user.setPassword(util.encrypt("pass", "Le1234567"));
            UserDAO userDAO = UserDAOImpl.getInstance();
            boolean real = userDAO.signUp(user);
            assertTrue(real);
        } catch (DuplicateTelephoneException | DuplicateEmailException
                | InvalidKeySpecException | NoSuchAlgorithmException
                | BadPaddingException | InvalidKeyException
                | NoSuchPaddingException | InvalidAlgorithmParameterException
                | IllegalBlockSizeException | IOException e) {
            logger.error(e);
        }
    }

    @Test
    public void testSignInFalse() {
        User user = new User();
        user.setEmail("ilyagalica@gmail.com");
        user.setPassword("Le1234567");
        UserDAO userDAO = UserDAOImpl.getInstance();
        try {
            boolean real = userDAO.signUp(user);
            assertFalse(real);
        } catch (DuplicateTelephoneException | DuplicateEmailException e) {
            logger.error(e);
        }
    }

    @Test
    public void testSignUp() {
        User user = new User();
        user.setEmail("firstDeleteTestEmail@mail.ru");
        user.setName("test");
        user.setSurname("surTest");
        user.setPatronymic("patTest");
        user.setTelephone("+375(29)678-65-76");
        user.setPassword("1234561");
        user.setJobPlace("No");
        UserDAO userDAO = UserDAOImpl.getInstance();
        try {
            assertTrue(userDAO.signUp(user));
        } catch (DuplicateTelephoneException | DuplicateEmailException e) {
            logger.error(e);
        }
    }

    @Test
    public void testChangePersonalInfo() {
        User user = new User();
        user.setEmail("firstDeleteTestEmail@mail.ru");
        user.setName("test");
        user.setSurname("surTest");
        user.setPatronymic("patTest");
        user.setTelephone("+375(29)678-65-76");
        user.setPassword("1234561");
        user.setJobPlace("No");
        UserDAO userDAO = UserDAOImpl.getInstance();
        User real = null;
        if (userDAO.changePersonalInfo("secondDeleteTestEmail@mail.ru", "email", user).isPresent()) {
            real = userDAO
                    .changePersonalInfo("secondDeleteTestEmail@mail.ru",
                            "email", user).get();
        }
        user.setEmail("secondDeleteTestEmail@mail.ru");
        assertEquals(real, user);

    }

    @AfterClass
    void deleteInserted() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM user WHERE email = ?");
            statement.setString(1, "secondDeleteTestEmail@mail.ru");
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
    }
}