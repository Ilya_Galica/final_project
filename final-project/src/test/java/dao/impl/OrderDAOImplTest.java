package dao.impl;

import dao.ConnectionPool;
import dao.OrderDAO;
import dao.entity.exception.OrderNotCreateException;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import helper.ServletContextConnector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.testng.Assert.*;

public class OrderDAOImplTest {
    private final static Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void setUpClass(){
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testCreateOrder() {
        User user = new User();
        user.setEmail("someNewestEmail@mail.ru");
        user.setName("Kevin");
        user.setSurname("Render");
        user.setPatronymic("Daae");
        user.setTelephone("+375(29)189-81-36");
        Hotelroom hotelroom = new Hotelroom();
        hotelroom.setNumber(9);
        Hotel hotel = new Hotel();
        hotel.setId(2);
        hotel.setName("Via Amsterdam");
        hotelroom.setHotel(hotel);
        String arrival = "2019-09-25";
        String departure = "2019-09-26";
        OrderDAO orderDAO = OrderDAOImpl.getInstance();
        Order order = new Order();
        order.setArrivalData(arrival);
        order.setDepartureData(departure);
        order.setUser(user);
        order.setHotelroom(hotelroom);
        Order actual = null;
        try {
            if(orderDAO.createOrder(order).isPresent()) {
                actual = orderDAO.createOrder(order).get();
            }
        } catch (OrderNotCreateException e) {
            logger.error(e);
        }
        assertNotNull(actual);
    }

    @AfterClass
    void deleteInserted() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM `order` WHERE arrival_data = ?" +
                    " and departure_data = ? and owner = ? and hotelroom = ?");
            statement.setString(1, "2019-09-25");
            statement.setString(2, "2019-09-26");
            statement.setInt(3, 186);
            statement.setInt(4, 9);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
    }
}