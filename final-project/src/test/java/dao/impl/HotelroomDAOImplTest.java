package dao.impl;

import dao.HotelDAO;
import dao.HotelroomDAO;
import dao.entity.HotelSerchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import helper.ServletContextConnector;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

public class HotelroomDAOImplTest {
    HotelroomDAOImpl hotelroomDAO = HotelroomDAOImpl.getInstance();

    @BeforeClass
    public static void setUpClass() {
        ServletContextConnector.setUpServletContext();
    }


    @Test
    public void testShowAll() {
        HotelSerchDataContainer container = new HotelSerchDataContainer();
        container.setCity("Minsk");
        container.setArrivalData("2019-08-13");
        container.setDepartureData("2019-08-15");
        container.setPeopleCapacity(2);
        Pagination pagination = new Pagination();
        pagination.setCurrentPage(1);
        List<Hotelroom> actualList = hotelroomDAO.showAll(container, pagination);
        Hotelroom actual = actualList.get(0);
        Hotelroom expected = new Hotelroom();
        expected.setComfort("halflux");
        expected.setPeopleCapacity(2);
        expected.setPrice(25);
        expected.setId(10);
        expected.setNumber(10);
        expected.setImageURL("images/1.jpg");
        expected.setDescription("В распоряжении гостей каждого номера гостиная зона");
        expected.setDescriptionEn("At guests ' disposal in each room a Seating area");
        Hotel hotel = new Hotel();
        hotel.setName("Agata");
        hotel.setCity("Minsk");
        hotel.setId(1);
        expected.setHotel(hotel);
        assertEquals(actual,expected);

    }

    @Test
    public void testGetAmountOfValidHotelrooms() {
        HotelSerchDataContainer container = new HotelSerchDataContainer();
        container.setCity("Minsk");
        container.setArrivalData("2019-08-13");
        container.setDepartureData("2019-08-13");
        container.setPeopleCapacity(2);
        int actual = hotelroomDAO.getAmountOfValidHotelrooms(container);
        int expected = 5;
        assertEquals(actual, expected);
    }

    @Test
    public void testGetDayAmount() {
        int actual = hotelroomDAO.getDayAmount("2019-08-13", "2019-08-15");
        int expected = 2;
        assertEquals(actual, expected);
    }
}