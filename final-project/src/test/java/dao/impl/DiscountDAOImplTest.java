package dao.impl;

import dao.entity.impl.Discount;
import helper.ServletContextConnector;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class DiscountDAOImplTest {
    @BeforeClass
    public static void setUpClass() {
        ServletContextConnector.setUpServletContext();
    }

    @Test
    public void testGetDiscountList() {
        DiscountDAOImpl discountDAO = DiscountDAOImpl.getInstance();
        List<Discount> discountList = discountDAO.getDiscountList();
        Discount actual = discountList.get(0);
        Discount expected = new Discount();
        expected.setUserJobPlace("Школа");
        assertEquals(actual, expected);
    }
}