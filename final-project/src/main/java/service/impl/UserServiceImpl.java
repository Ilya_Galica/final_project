package service.impl;

import dao.entity.Entity;
import dao.entity.exception.DuplicateEmailException;
import dao.entity.exception.DuplicateTelephoneException;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.User;
import dao.impl.UserDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;
import service.exception.ServiceException;
import service.util.CryptoUtil;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

import static service.util.ServiceUtil.validateSignIn;
import static service.util.ServiceUtil.validateSignUp;

public class UserServiceImpl implements UserService {
    private final static Logger logger = LogManager.getLogger();
    private final static UserDAOImpl userDAO = UserDAOImpl.getInstance();
    private final static CryptoUtil cryptoUtil = CryptoUtil.getInstance();
    private final static String KEY = "pass";
    private final static UserServiceImpl instance = new UserServiceImpl();

    private UserServiceImpl() {

    }

    public static UserServiceImpl getInstance() {
        return instance;
    }

    @Override
    public User signIn(String email, String password) throws ServiceException {
        User user = null;
        try {
            validateSignIn(email,password);
            password = cryptoUtil.encrypt(KEY, password);
            if (userDAO.signIn(email, password).isPresent()) {
                user = userDAO.signIn(email, password).get();
                user.setPassword(cryptoUtil.decrypt(KEY, user.getPassword()));
                return user;
            } else {
                return userDAO.signIn(email, password).orElse(new User());
            }
        } catch (SuchUserNotFoundException e) {
            logger.error(e);
            throw new ServiceException("Such User Not Found, check pass or email");
        } catch (InvalidKeySpecException | UnsupportedEncodingException | BadPaddingException
                | NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException
                | NoSuchPaddingException | IllegalBlockSizeException e) {
            logger.error(e);
            throw new ServiceException("Problems whit password");
        } catch (IOException e) {
            logger.error(e);
        }
        return user;
    }

    @Override
    public boolean signUp(User user) throws ServiceException {
        try {
            validateSignUp(user);
            user.setPassword(cryptoUtil.encrypt(KEY, user.getPassword()));
            return userDAO.signUp(user);
        } catch (DuplicateTelephoneException e) {
            logger.error(e);
            throw new ServiceException("Такой телефон (" + user.getTelephone() + ") уже зарегистрирован");
        } catch (DuplicateEmailException e) {
            logger.error(e);
            throw new ServiceException("Такой email (" + user.getEmail() + ") уже зарегистрирован");
        } catch (InvalidKeySpecException | UnsupportedEncodingException | BadPaddingException
                | NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException
                | NoSuchPaddingException | IllegalBlockSizeException e) {
            logger.error(e);
            throw new ServiceException("Problems whit password of this user " + user.getEmail());
        }
    }

    @Override
    public User changePersonalInfo(String newParam, String type, User user) throws ServiceException {
        if (type.equals("job")) {
            type = "job_place";
        }
        Optional<User> returnedUser = UserDAOImpl.getInstance().changePersonalInfo(newParam, type, user);
        if (returnedUser.isPresent()) {
            return returnedUser.get();
        } else {
            throw new ServiceException("Пользователь пуст");
        }
    }

    @Override
    public List<Entity> findAll() {
        return UserDAOImpl.getInstance().findAll();
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = UserDAOImpl.getInstance().findById(id);
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new ServiceException("No such user");
        }
    }

    @Override
    public boolean delete(Entity entity) {
        return UserDAOImpl.getInstance().delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return false;
    }


}
