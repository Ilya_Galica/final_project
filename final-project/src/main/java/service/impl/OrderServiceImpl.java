package service.impl;

import dao.DiscountDAO;
import dao.HotelroomDAO;
import dao.OrderDAO;
import dao.entity.Entity;
import dao.entity.exception.OrderNotCreateException;
import dao.entity.impl.Discount;
import dao.entity.impl.Order;
import dao.impl.AbstractDAO;
import dao.impl.DiscountDAOImpl;
import dao.impl.HotelroomDAOImpl;
import dao.impl.OrderDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.OrderService;
import service.util.ServiceUtil;
import service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

import static service.util.ServiceUtil.validateUserInput;

public class OrderServiceImpl implements OrderService {
    private final static Logger logger = LogManager.getLogger();
    private OrderDAO orderDAO = OrderDAOImpl.getInstance();
    private final static OrderServiceImpl instance = new OrderServiceImpl();


    private OrderServiceImpl() {

    }

    public static OrderServiceImpl getInstance() {
        return instance;
    }

    @Override
    public Optional<Order> createOrder(Order order) throws ServiceException {
        try {
            HotelroomDAO hotelroomDAO = HotelroomDAOImpl.getInstance();
            validateUserInput(order.getUser());
            String jobPlace = order.getUser().getJobPlace();
            DiscountDAO discountDAO = DiscountDAOImpl.getInstance();
            List<Discount> discountList = discountDAO.getDiscountList();
            for (Discount discount : discountList) {
                if (jobPlace.toUpperCase()
                        .contains(discount
                                .getUserJobPlace().toUpperCase())) {
                    order.getUser().setJobPlace(discount.getUserJobPlace());
                }
            }
            int dayAmount
                    = hotelroomDAO
                    .getDayAmount(order.getArrivalData(),
                            order.getDepartureData());
            int totalPrice = ((dayAmount != 0) ? dayAmount : 1) * order.getHotelroom().getPrice();
            order.setTotalPrice(totalPrice);
            order.setArrivalData(ServiceUtil.reverseDateForSQL(order.getArrivalData()));
            order.setDepartureData(ServiceUtil.reverseDateForSQL(order.getDepartureData()));

            return orderDAO.createOrder(order);
        } catch (OrderNotCreateException e) {
            logger.error(e);
            throw new ServiceException("Order hadn't been created");
        }
    }

    @Override
    public void deleteOrder(Order order) throws ServiceException {
        AbstractDAO dao = OrderDAOImpl.getInstance();
        if (!dao.delete(order)) {
            throw new ServiceException("Order hadn't been deleted");
        }
    }

    @Override
    public List<Entity> findAll() {
        return OrderDAOImpl.getInstance().findAll();
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = OrderDAOImpl.getInstance().findById(id);
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new ServiceException("No such order");
        }
    }

    @Override
    public boolean delete(Entity entity) {
        return OrderDAOImpl.getInstance().delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return OrderDAOImpl.getInstance().create(entity);
    }


}
