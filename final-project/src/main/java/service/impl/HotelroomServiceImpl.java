package service.impl;

import dao.HotelroomDAO;
import dao.entity.Entity;
import dao.entity.HotelSerchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotelroom;
import dao.impl.DiscountDAOImpl;
import dao.impl.HotelroomDAOImpl;
import service.HotelroomService;
import service.exception.ServiceException;
import service.util.ServiceUtil;

import java.util.List;
import java.util.Optional;

public class HotelroomServiceImpl implements HotelroomService {
    private HotelroomDAO hotelroomDAO = HotelroomDAOImpl.getInstance();
    private ServiceUtil serviceUtil = ServiceUtil.getInstance();
    private final static HotelroomServiceImpl instance = new HotelroomServiceImpl();

    private HotelroomServiceImpl() {

    }

    public static HotelroomServiceImpl getInstance() {
        return instance;
    }


    @Override
    public List<Hotelroom> showAll(HotelSerchDataContainer container, Pagination pagination) {
        String anotherLanguageCity
                = serviceUtil.getAnotherLanguageCity(container.getCity());
        if (anotherLanguageCity != null) {
            container.setCity(anotherLanguageCity);
        }
        return hotelroomDAO.showAll(container, pagination);
    }

    @Override
    public int getAmountOfValidHotelrooms(HotelSerchDataContainer container) {
        container
                .setArrivalData(ServiceUtil.reverseDateForSQL(container.getArrivalData()));
        container
                .setDepartureData(ServiceUtil.reverseDateForSQL(container.getDepartureData()));
        return hotelroomDAO.getAmountOfValidHotelrooms(container);
    }

    @Override
    public int getDayAmount(String arrivalData, String departureData) {
        if (hotelroomDAO.getDayAmount(arrivalData, departureData) == 0) {
            return 1;
        } else {
            return hotelroomDAO.getDayAmount(arrivalData, departureData);
        }
    }

    @Override
    public List<Entity> findAll() {
        return HotelroomDAOImpl.getInstance().findAll();
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = HotelroomDAOImpl.getInstance().findById(id);
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new ServiceException("No such hotelroom");
        }
    }

    @Override
    public boolean delete(Entity entity) {
        return HotelroomDAOImpl.getInstance().delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return HotelroomDAOImpl.getInstance().create(entity);
    }


}
