package service.impl;

import dao.entity.Entity;
import dao.impl.DiscountDAOImpl;
import service.DiscountService;
import service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public class DiscountServiceImpl implements DiscountService {

    private final static DiscountServiceImpl instance = new DiscountServiceImpl();

    private DiscountServiceImpl() {

    }

    public static DiscountServiceImpl getInstance() {
        return instance;
    }

    @Override
    public List<Entity> findAll() {
        return DiscountDAOImpl.getInstance().findAll();
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = DiscountDAOImpl.getInstance().findById(id);
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new ServiceException("No such discount");
        }
    }

    @Override
    public boolean delete(Entity entity) {
        return DiscountDAOImpl.getInstance().delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return DiscountDAOImpl.getInstance().create(entity);
    }

}
