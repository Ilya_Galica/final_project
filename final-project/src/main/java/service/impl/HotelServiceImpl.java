package service.impl;

import dao.entity.Entity;
import dao.impl.HotelDAOImpl;
import service.HotelService;
import service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public class HotelServiceImpl implements HotelService {
    private final static HotelServiceImpl instance = new HotelServiceImpl();

    private HotelServiceImpl() {

    }

    public static HotelServiceImpl getInstance() {
        return instance;
    }

    @Override
    public List<Entity> findAll() {
        return HotelDAOImpl.getInstance().findAll();
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = HotelDAOImpl.getInstance().findById(id);
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new ServiceException("No such hotel");
        }
    }

    @Override
    public boolean delete(Entity entity) {
        return HotelDAOImpl.getInstance().delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return HotelDAOImpl.getInstance().create(entity);
    }
}
