package service;

import dao.entity.Entity;
import dao.entity.HotelSerchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotelroom;
import service.exception.ServiceException;

import java.util.List;

public interface HotelroomService {

    List<Hotelroom> showAll(HotelSerchDataContainer container, Pagination pagination);

    int getAmountOfValidHotelrooms(HotelSerchDataContainer container);

    int getDayAmount(String arrivalData, String departureData);

    List<Entity> findAll();

    Entity findById(int id) throws ServiceException;

    boolean delete(Entity entity);

    boolean create(Entity entity);
}
