package service.util;

import dao.entity.impl.User;
import service.exception.ServiceException;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServiceUtil {
    private final static Map<String, String> cityMap = new HashMap<>();
    private final static ServiceUtil instance = new ServiceUtil();

    public static ServiceUtil getInstance() {
        return instance;
    }

    private ServiceUtil() {
        cityMap.put("минск", "minsk");
        cityMap.put("амстердам", "amsterdam");
    }

    public static String reverseDateForSQL(String date) {
        return date.replaceAll("((\\d)*)/((\\d)*)/((\\d)*)", "$5-$3-$1");
    }

    public String getAnotherLanguageCity(String city) {
        return cityMap.get(city.toLowerCase());
    }

    public static boolean validateParameter(String param, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(param);
        return matcher.matches();
    }

    public static boolean validateSignIn(String email, String password) throws ServiceException {
        if (validateParameter(password, "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(\\w){6,30}")
                || password.equals("admin")) {
            return true;
        } else {
            throw new ServiceException("Invalid password: " + password);
        }
    }

    public static boolean validateSignUp(User user) throws ServiceException {
        if (!validateParameter(user.getTelephone(),
                "((80)?|(\\+375)?)(\\(\\d{2}\\)|\\d{2})((\\d{3}-\\d{2}-\\d{2})|\\d{7})")) {
            throw new ServiceException("Invalid telephone: " + user.getTelephone());
        }
        if (!validateParameter(user.getName(), "[a-zA-Zа-яА-Я]{2,}")) {
            throw new ServiceException("Invalid name: " + user.getName());
        }
        if (!validateParameter(user.getSurname(), "[a-zA-Zа-яА-Я]{2,}")) {
            throw new ServiceException("Invalid surname: " + user.getSurname());
        }
        if (!validateParameter(user.getPatronymic(), "[a-zA-Zа-яА-Я]{2,}")) {
            throw new ServiceException("Invalid patronymic: " + user.getPatronymic());
        }
        if (!validateParameter(user.getPassword(), "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(\\w){6,30}")) {
            throw new ServiceException("Invalid password: " + user.getPassword());
        }
        user.setJobPlace(user.getJobPlace().replaceAll("\\'", "")
                .replaceAll("\\\"", "")
                .replaceAll("\\)", ""));
        return true;
    }

    public static boolean validateUserInput(User user) throws ServiceException {
        if (!validateParameter(user.getTelephone(),
                "((80)?|(\\+375)?)(\\(\\d{2}\\)|\\d{2})((\\d{3}-\\d{2}-\\d{2})|\\d{7})")) {
            throw new ServiceException("Invalid telephone: " + user.getTelephone());
        }
        if (!validateParameter(user.getName(), "[a-zA-Zа-яА-Я]{2,}")) {
            throw new ServiceException("Invalid name: " + user.getName());
        }
        if (!validateParameter(user.getSurname(), "[a-zA-Zа-яА-Я]{2,}")) {
            throw new ServiceException("Invalid surname: " + user.getSurname());
        }
        if (!validateParameter(user.getPatronymic(), "[a-zA-Zа-яА-Я]{2,}")) {
            throw new ServiceException("Invalid patronymic: " + user.getPatronymic());
        }
        user.setJobPlace(user.getJobPlace().replaceAll("\\'", "")
                .replaceAll("\\\"", "")
                .replaceAll("\\)", ""));
        return true;
    }
}
