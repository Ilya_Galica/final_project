package dao.command;

import dao.command.impl.*;

import java.util.HashMap;
import java.util.Map;

public class DirectorCommandDAO {
    private Map<String, CommandDAO> map = new HashMap<>();
    private final static DirectorCommandDAO instance = new DirectorCommandDAO();

    public static DirectorCommandDAO getInstance() {
        return instance;
    }

    private DirectorCommandDAO() {
        map.put("hotelroom", new HotelroomCommandDAO());
        map.put("hotel", new HotelCommandDAO());
        map.put("order", new OrderCommandDAO());
        map.put("discount", new DiscountCommandDAO());
        map.put("user", new UserCommandDAO());
    }

    public CommandDAO getCommand(String commandKey) {
        return map.get(commandKey);
    }
}
