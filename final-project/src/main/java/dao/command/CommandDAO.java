package dao.command;

import dao.entity.Entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface CommandDAO {
    List<Entity> findAll(ResultSet resultSet) throws SQLException;

    Optional<Entity> findById(ResultSet resultSet) throws SQLException;

    boolean delete(Entity entity, Connection connection) throws SQLException;

    boolean create(Entity entity, Connection connection) throws SQLException;
}
