package dao.command.impl;

import dao.ConnectionPool;
import dao.command.CommandDAO;
import dao.entity.Entity;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import dao.impl.HotelDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static dao.util.DAOUtil.isSuchRecordExist;

public class HotelroomCommandDAO implements CommandDAO {
    private HotelDAOImpl hotelDao = HotelDAOImpl.getInstance();
    private ConnectionPool pool = ConnectionPool.getInstance();
    private String className = Hotelroom.class.getSimpleName();
    private final static Logger logger = LogManager.getLogger();

    @Override
    public List<Entity> findAll(ResultSet resultSet) throws SQLException {
        List<Entity> entityList = new ArrayList<>();
        while (resultSet.next()) {
            Hotelroom hotelroom = new Hotelroom();
            hotelroom.setId(resultSet.getInt("id"));
            hotelroom.setNumber(resultSet.getInt("number"));
            Hotel hotel = null;
            if (hotelDao.findById(resultSet.getInt("hotel")).isPresent()) {
                hotel = (Hotel) hotelDao.findById(resultSet.getInt("hotel")).get();
            }
            hotelroom.setHotel(hotel);
            hotelroom.setPrice(resultSet.getInt("price"));
            hotelroom.setPeopleCapacity(resultSet.getInt("capacity"));
            hotelroom.setComfort(resultSet.getString("comfort"));
            hotelroom.setImageURL(resultSet.getString("image_url"));
            hotelroom.setDescription(resultSet.getString("description"));
            hotelroom.setDescriptionEn(resultSet.getString("description_en"));
            entityList.add(hotelroom);
        }
        return entityList;
    }

    @Override
    public Optional<Entity> findById(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            Hotelroom hotelroom = new Hotelroom();
            hotelroom.setId(resultSet.getInt("id"));
            hotelroom.setNumber(resultSet.getInt("number"));
            Hotel hotel = (Hotel) hotelDao.findById(resultSet.getInt("hotel")).get();
            hotelroom.setHotel(hotel);
            hotelroom.setPrice(resultSet.getInt("price"));
            hotelroom.setPeopleCapacity(resultSet.getInt("capacity"));
            hotelroom.setComfort(resultSet.getString("comfort"));
            hotelroom.setImageURL(resultSet.getString("image_url"));
            hotelroom.setDescription(resultSet.getString("description"));
            hotelroom.setDescriptionEn(resultSet.getString("description_en"));
            return Optional.of(hotelroom);
        }
        return Optional.empty();
    }

    @Override
    public boolean delete(Entity entity, Connection connection) {
        PreparedStatement statement = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement("DELETE FROM `order` WHERE owner = ?;");
            statement.setInt(1, entity.getId());
            int orderDelete = statement.executeUpdate();
            statement = connection.prepareStatement("DELETE FROM hotelroom WHERE id = ?;");
            statement.setInt(1, entity.getId());
            int hotelroomDelete = statement.executeUpdate();
            if (hotelroomDelete == 1) {
                connection.commit();
                return true;
            }
        } catch (SQLException e) {
            logger.error(e);
            pool.connectionRollback(connection);
        } finally {
            pool.closePreparedStatement(statement);
        }
        return false;
    }

    @Override
    public boolean create(Entity entity, Connection connection) throws SQLException {
        Hotelroom hotelroom = (Hotelroom) entity;
//        isSuchRecordExist(connection,id, className);
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO hotelroom " +
                "(number, hotel, price, capacity, comfort, image_url, description, description_en)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?);")) {
            statement.setInt(1, hotelroom.getNumber());
            statement.setInt(2, hotelroom.getHotel().getId());
            statement.setInt(3, hotelroom.getPrice());
            statement.setInt(4, hotelroom.getPeopleCapacity());
            statement.setString(5, hotelroom.getComfort());
            statement.setString(6, hotelroom.getImageURL());
            statement.setString(7, hotelroom.getDescription());
            statement.setString(8, hotelroom.getDescriptionEn());
            return statement.executeUpdate() == 1;
        }
    }
}
