package dao.command.impl;

import dao.ConnectionPool;
import dao.command.CommandDAO;
import dao.entity.Entity;
import dao.entity.impl.Hotel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class HotelCommandDAO implements CommandDAO {
    private ConnectionPool pool = ConnectionPool.getInstance();
    private String className = Hotel.class.getSimpleName();
    private final static Logger logger = LogManager.getLogger();

    @Override
    public List<Entity> findAll(ResultSet resultSet) throws SQLException {
        List<Entity> entityList = new ArrayList<>();
        while (resultSet.next()) {
            Hotel hotel = new Hotel();
            hotel.setId(resultSet.getInt("id"));
            hotel.setName(resultSet.getString("name"));
            hotel.setCity(resultSet.getString("city"));
            hotel.setCountry(resultSet.getString("country"));
            entityList.add(hotel);
        }
        return entityList;
    }

    @Override
    public Optional<Entity> findById(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            Hotel hotel = new Hotel();
            hotel.setId(resultSet.getInt("id"));
            hotel.setName(resultSet.getString("name"));
            hotel.setCity(resultSet.getString("city"));
            hotel.setCountry(resultSet.getString("country"));
            return Optional.of(hotel);
        }
        return Optional.empty();
    }

    @Override
    public boolean delete(Entity entity, Connection connection) {
        PreparedStatement statement = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement("DELETE FROM `order` WHERE hotelroom IN" +
                    " (SELECT id FROM hotelroom WHERE hotel=?);");
            statement.setInt(1, entity.getId());
            statement.executeUpdate();
            statement = connection.prepareStatement("DELETE FROM hotelroom WHERE hotel = ?;");
            statement.setInt(1, entity.getId());
            statement.executeUpdate();
            statement = connection.prepareStatement("DELETE FROM hotel WHERE id = ?;");
            statement.setInt(1, entity.getId());
            int hotelDelete = statement.executeUpdate();
            if (hotelDelete == 1) {
                connection.commit();
                return true;
            }
        } catch (SQLException e) {
            logger.error(e);
            pool.connectionRollback(connection);
        } finally {
            pool.closePreparedStatement(statement);
        }
        return false;
    }

    public boolean create(Entity entity, Connection connection) throws SQLException {
        Hotel hotel = (Hotel) entity;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO hotel " +
                    "(name,  city, country) VALUES ( ?, ?, ?);");
            statement.setString(1, hotel.getName());
            statement.setString(2, hotel.getCity());
            statement.setString(3, hotel.getCountry());
            return statement.executeUpdate() == 1;
        } finally {
            pool.closePreparedStatement(statement);
        }
    }
}
