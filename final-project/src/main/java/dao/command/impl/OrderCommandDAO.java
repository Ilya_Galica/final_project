package dao.command.impl;

import dao.ConnectionPool;
import dao.command.CommandDAO;
import dao.entity.Entity;
import dao.entity.impl.Discount;
import dao.entity.impl.Hotelroom;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import dao.impl.DiscountDAOImpl;
import dao.impl.HotelroomDAOImpl;
import dao.impl.UserDAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static dao.util.DAOUtil.simpleDelete;

public class OrderCommandDAO implements CommandDAO {
    private static HotelroomDAOImpl hotelroomDAO = HotelroomDAOImpl.getInstance();
    private static UserDAOImpl userDAO;
    private static DiscountDAOImpl discountDAO ;
    private ConnectionPool pool = ConnectionPool.getInstance();
    private String className = Order.class.getSimpleName();

    @Override
    public List<Entity> findAll(ResultSet resultSet) throws SQLException {
        List<Entity> entityList = new ArrayList<>();

        while (resultSet.next()) {
            Order order = new Order();
            order.setId(resultSet.getInt("id"));
            Discount discount = null;
            discountDAO = DiscountDAOImpl.getInstance();
            if (discountDAO
                    .findById(resultSet.getInt("discount")).isPresent()) {
                discount = (Discount) discountDAO
                        .findById(resultSet.getInt("discount")).get();
            }
            order.setDiscount(discount);
            User user = null;
            userDAO = UserDAOImpl.getInstance();
            if (userDAO
                    .findById(resultSet.getInt("owner")).isPresent()) {
                user = (User) userDAO
                        .findById(resultSet.getInt("owner")).get();
            }
            order.setUser(user);
            order.setArrivalData(resultSet.getString("arrival_data"));
            order.setDepartureData(resultSet.getString("departure_data"));
            Hotelroom hotelroom = null;
            if (hotelroomDAO
                    .findById(resultSet.getInt("hotelroom")).isPresent()) {
                hotelroom = (Hotelroom) hotelroomDAO
                        .findById(resultSet.getInt("hotelroom")).get();
            }
            order.setHotelroom(hotelroom);
            order.setTotalPrice(resultSet.getInt("total_price"));
            entityList.add(order);
        }
        return entityList;
    }

    @Override
    public Optional<Entity> findById(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            Order order = new Order();
            order.setId(resultSet.getInt("id"));
            Discount discount = null;
            discountDAO = DiscountDAOImpl.getInstance();
            if (discountDAO
                    .findById(resultSet.getInt("discount")).isPresent()) {
                discount = (Discount) discountDAO
                        .findById(resultSet.getInt("discount")).get();
            }
            order.setDiscount(discount);
            User user = null;
            userDAO = UserDAOImpl.getInstance();
            if (userDAO
                    .findById(resultSet.getInt("owner")).isPresent()) {
                user = (User) userDAO
                        .findById(resultSet.getInt("owner")).get();
            }
            order.setUser(user);
            order.setArrivalData(resultSet.getString("arrival_data"));
            order.setDepartureData(resultSet.getString("departure_data"));
            Hotelroom hotelroom
                    = (Hotelroom) hotelroomDAO
                    .findById(resultSet.getInt("hotelroom")).get();
            order.setHotelroom(hotelroom);
            order.setTotalPrice(resultSet.getInt("total_price"));
            return Optional.of(order);
        }
        return Optional.empty();
    }

    @Override
    public boolean delete(Entity entity, Connection connection) throws SQLException {
        return simpleDelete(entity, connection, className);
    }

    @Override
    public boolean create(Entity entity, Connection connection) throws SQLException {
        Order order = (Order) entity;
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `order` " +
                "( discount, owner, arrival_data, departure_data, hotelroom, total_price)" +
                " VALUES ( ? ,?, ?, ?, ?, ?);")) {
            statement.setInt(1, order.getDiscount().getId());
            statement.setInt(2, order.getUser().getId());
            statement.setString(3, order.getArrivalData());
            statement.setString(4, order.getDepartureData());
            statement.setInt(5, order.getHotelroom().getId());
            statement.setInt(6, order.getTotalPrice());
            return statement.executeUpdate() == 1;
        }
    }
}
