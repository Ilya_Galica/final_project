package dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class ConnectionPool {
    private static ConnectionPool instance;

    private final static Logger logger = LogManager.getLogger();

    public static ConnectionPool getInstance() {
        Lock locker = new ReentrantLock();
        locker.lock();
        try {
            if (instance == null) {
                instance = new ConnectionPool();
            }
        } finally {
            locker.unlock();
        }
        return instance;
    }

    private Connection connection;

    public Connection getConnection() {
        Context context;
        try {
            context = (Context) new InitialContext().lookup("java:comp/env");
            DataSource dataSource = (DataSource) context.lookup("jdbc/hotel_booking");
            connection = dataSource.getConnection();
            return connection;
        } catch (NamingException | SQLException e) {
            logger.error(e);
        }
        return connection;
    }

    public void closeConnection(Connection currentConnection) {
        try {
            if (currentConnection != null) {
                currentConnection.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }

    }

    public void closePreparedStatement(PreparedStatement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    public void closeResultSet(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    public void connectionRollback(Connection connection){
        try {
            connection.rollback();
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    public void closeAll(ResultSet resultSet, PreparedStatement statement, Connection connection){
        closeResultSet(resultSet);
        closePreparedStatement(statement);
        closeConnection(connection);
    }

}
