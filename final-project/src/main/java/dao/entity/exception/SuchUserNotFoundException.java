package dao.entity.exception;

public class SuchUserNotFoundException extends Exception {
    public SuchUserNotFoundException() {
    }

    public SuchUserNotFoundException(String s) {
        super(s);
    }

    public SuchUserNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SuchUserNotFoundException(Throwable throwable) {
        super(throwable);
    }

    public SuchUserNotFoundException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
