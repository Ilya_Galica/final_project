package dao.entity.exception;

public class DuplicateEmailException extends Exception{
    public DuplicateEmailException() {
    }

    public DuplicateEmailException(String s) {
        super(s);
    }

    public DuplicateEmailException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DuplicateEmailException(Throwable throwable) {
        super(throwable);
    }

    public DuplicateEmailException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
