package dao.entity;

public class HotelSerchDataContainer {
    private String arrivalData;
    private String departureData;
    private String city;
    private int peopleCapacity;

    public HotelSerchDataContainer(String arrivalData, String departureData, String city, int peopleCapacity) {
        this.arrivalData = arrivalData;
        this.departureData = departureData;
        this.city = city;
        this.peopleCapacity = peopleCapacity;
    }

    public HotelSerchDataContainer() {
    }

    public String getArrivalData() {
        return arrivalData;
    }

    public void setArrivalData(String arrivalData) {
        this.arrivalData = arrivalData;
    }

    public String getDepartureData() {
        return departureData;
    }

    public void setDepartureData(String departureData) {
        this.departureData = departureData;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPeopleCapacity() {
        return peopleCapacity;
    }

    public void setPeopleCapacity(int peopleCapacity) {
        this.peopleCapacity = peopleCapacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HotelSerchDataContainer that = (HotelSerchDataContainer) o;

        if (peopleCapacity != that.peopleCapacity) return false;
        if (arrivalData != null ? !arrivalData.equals(that.arrivalData) : that.arrivalData != null) return false;
        if (departureData != null ? !departureData.equals(that.departureData) : that.departureData != null)
            return false;
        return city != null ? city.equals(that.city) : that.city == null;
    }

    @Override
    public int hashCode() {
        int result = arrivalData != null ? arrivalData.hashCode() : 0;
        result = 31 * result + (departureData != null ? departureData.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + peopleCapacity;
        return result;
    }

    @Override
    public String toString() {
        return "HotelSerchDataContainer{" +
                "arrivalData='" + arrivalData + '\'' +
                ", departureData='" + departureData + '\'' +
                ", city='" + city + '\'' +
                ", peopleCapacity='" + peopleCapacity + '\'' +
                '}';
    }
}
