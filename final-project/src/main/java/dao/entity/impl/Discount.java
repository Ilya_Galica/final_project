package dao.entity.impl;

import dao.entity.Entity;


public class Discount extends Entity {
    private String userJobPlace;
    private int discountValueForJobPlace;

    public String getUserJobPlace() {
        return userJobPlace;
    }

    public void setUserJobPlace(String userJobPlace) {
        this.userJobPlace = userJobPlace;
    }

    public int getDiscountValueForJobPlace() {
        return discountValueForJobPlace;
    }

    public void setDiscountValueForJobPlace(int discountValueForJobPlace) {
        this.discountValueForJobPlace = discountValueForJobPlace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Discount discount = (Discount) o;

        if (discountValueForJobPlace != discount.discountValueForJobPlace) return false;
        return userJobPlace != null ? userJobPlace.equals(discount.userJobPlace) : discount.userJobPlace == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (userJobPlace != null ? userJobPlace.hashCode() : 0);
        result = 31 * result + discountValueForJobPlace;
        return result;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "userJobPlace='" + userJobPlace + '\'' +
                ", discountValueForJobPlace='" + discountValueForJobPlace + '\'' +
                "} " + super.toString();
    }
}
