package dao;

import dao.entity.HotelSerchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotelroom;

import java.util.List;

public interface HotelroomDAO {

    List<Hotelroom> showAll(HotelSerchDataContainer container, Pagination pagination);

    int getAmountOfValidHotelrooms(HotelSerchDataContainer container);

    int getDayAmount(String arrivalData, String departureData);
}
