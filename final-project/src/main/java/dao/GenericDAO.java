package dao;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface GenericDAO<T extends Serializable> {
    Optional<T> findById(int id);
    List<T> findAll();
    boolean delete(T entity);
    boolean create(T entity);
    Optional<T> update(T entity);
}
