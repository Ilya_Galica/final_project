package dao;

import dao.entity.exception.DuplicateEmailException;
import dao.entity.exception.DuplicateTelephoneException;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.User;

import java.util.Optional;

public interface UserDAO {
    Optional<User> signIn(String email, String password) throws SuchUserNotFoundException;

    boolean signUp(User user) throws DuplicateTelephoneException, DuplicateEmailException;

    Optional<User> changePersonalInfo(String newParam, String type,User user);
}
