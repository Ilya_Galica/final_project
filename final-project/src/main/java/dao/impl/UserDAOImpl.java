package dao.impl;

import dao.UserDAO;
import dao.ConnectionPool;
import dao.entity.UserRole;
import dao.entity.exception.DuplicateEmailException;
import dao.entity.exception.DuplicateTelephoneException;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class UserDAOImpl extends AbstractDAO implements UserDAO {
    private final static UserDAOImpl instance = new UserDAOImpl();

    private UserDAOImpl() {
        setClassName(User.class.getSimpleName().toLowerCase());
    }

    public static UserDAOImpl getInstance() {
        return instance;
    }

    private final static Logger logger = LogManager.getLogger();

    private final static String SIGN_IN = "SELECT * FROM user WHERE email= ? AND password= ?";
    private final static String SIGN_UP = "INSERT INTO user"
            + " (email, name, surname, patronymic, telephone, password, job_place)"
            + " VALUES(?,?,?,?,?,?,?)";
    private final static String EMAIL_DUPLICATE = "SELECT email FROM user where email=?";
    private final static String TELEPHONE_DUPLICATE = "SELECT  telephone FROM user WHERE telephone=?";

    private final static String UPDATE_SET = "UPDATE user set ";
    private final static String UPDATE_WHERE = " = ? where telephone = ? and email = ?";

    private final static String SELECT_FROM = "SELECT * FROM user ";
    private final static String SELECT_WHERE = " where email = ?";
    private final static String SELECT_WHERE_EMAIL = " where telephone = ?";


    private ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public Optional<User> signIn(String email, String password) throws SuchUserNotFoundException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .prepareStatement(SIGN_IN);
            statement.setString(1, email);
            statement.setString(2, password);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setEmail(resultSet.getString("email"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setPatronymic(resultSet.getString("patronymic"));
                user.setPassword(resultSet.getString("password"));
                user.setTelephone(resultSet.getString("telephone"));
                user.setJobPlace(resultSet.getString("job_place"));
                user.setRole(
                        (resultSet.getString("role")
                                .equalsIgnoreCase(String.valueOf(UserRole.ADMIN)))
                                ? UserRole.ADMIN
                                : UserRole.USER);
                return Optional.of(user);
            } else {
                throw new SuchUserNotFoundException();
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
        return Optional.empty();
    }

    @Override
    public boolean signUp(User user) throws DuplicateTelephoneException, DuplicateEmailException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            boolean notDuplicateEmail
                    = isNotDuplicateEmail(connection, user.getEmail());
            boolean notDuplicateTelephone
                    = isNotDuplicateTelephone(connection, user.getTelephone());
            if (notDuplicateEmail && notDuplicateTelephone) {
                statement = connection.prepareStatement(SIGN_UP);
                statement.setString(1, user.getEmail());
                statement.setString(2, user.getName());
                statement.setString(3, user.getSurname());
                statement.setString(5, user.getTelephone());
                statement.setString(4, user.getPatronymic());
                statement.setString(6, user.getPassword());
                statement.setString(7, user.getJobPlace());
                return statement.executeUpdate() == 1;
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
        return false;
    }

    @Override
    public Optional<User> changePersonalInfo(String newParam, String type, User user) {
        Connection connection = null;
        PreparedStatement statement = null;
        PreparedStatement statementSelect = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(UPDATE_SET + type + UPDATE_WHERE);
            statementSelect = connection.prepareStatement(SELECT_FROM + type + SELECT_WHERE);
            switch (type) {
                case "email":
                    statementSelect = connection.prepareStatement(SELECT_FROM + type + SELECT_WHERE_EMAIL);
                    updateParam(user, statement, newParam);
                    resultSet = getUserWithNewParamEmail(user, statementSelect);
                    if (resultSet.next()) {
                        user.setEmail(resultSet.getString("email"));
                    }
                    break;
                case "password":
                    updateParam(user, statement, newParam);
                    resultSet = getUserWithNewParam(user, statementSelect);
                    if (resultSet.next()) {
                        user.setPassword(resultSet.getString("password"));
                    }
                    break;
                case "telephone":
                    updateParam(user, statement, newParam);
                    resultSet = getUserWithNewParam(user, statementSelect);
                    if (resultSet.next()) {
                        user.setTelephone(resultSet.getString("telephone"));
                    }
                    break;
                case "job_place":
                    updateParam(user, statement, newParam);
                    resultSet = getUserWithNewParam(user, statementSelect);
                    if (resultSet.next()) {
                        user.setJobPlace(resultSet.getString("job_place"));
                    }
                    break;
            }
            connection.commit();
            return Optional.of(user);
        } catch (SQLException e) {
            logger.error(e);
            pool.connectionRollback(connection);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.closePreparedStatement(statementSelect);
            pool.closeConnection(connection);
        }
        return Optional.empty();
    }

    private boolean isNotDuplicateEmail(Connection connection, String email)
            throws DuplicateEmailException, SQLException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(EMAIL_DUPLICATE);
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                throw new DuplicateEmailException("This email is not available");
            } else {
                return true;
            }
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
        }
    }

    private boolean isNotDuplicateTelephone(Connection connection, String telephone)
            throws SQLException, DuplicateTelephoneException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(TELEPHONE_DUPLICATE);
            statement.setString(1, telephone);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                throw new DuplicateTelephoneException("This telephone is not available");
            } else {
                return true;
            }
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
        }
    }

    private void updateParam(User user,
                             PreparedStatement statement, String newParam) throws SQLException {
        statement.setString(1, newParam);
        statement.setString(2, user.getTelephone());
        statement.setString(3, user.getEmail());
        statement.executeUpdate();
    }

    private ResultSet getUserWithNewParam(User user, PreparedStatement statement) throws SQLException {
        statement.setString(1, user.getEmail());
        return statement.executeQuery();
    }

    private ResultSet getUserWithNewParamEmail(User user, PreparedStatement statement) throws SQLException {
        statement.setString(1, user.getTelephone());
        return statement.executeQuery();
    }


}
