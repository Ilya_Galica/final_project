package dao.impl;

import dao.HotelDAO;
import dao.entity.impl.Hotel;

public class HotelDAOImpl extends AbstractDAO implements HotelDAO {

    private HotelDAOImpl() {
        setClassName(Hotel.class.getSimpleName().toLowerCase());
    }

    public static HotelDAOImpl getInstance() {
        return instance;
    }

    private final static HotelDAOImpl instance = new HotelDAOImpl();
}
