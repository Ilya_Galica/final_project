package dao.impl;

import dao.ConnectionPool;
import dao.HotelroomDAO;
import dao.entity.HotelSerchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HotelroomDAOImpl extends AbstractDAO implements HotelroomDAO {

    private HotelroomDAOImpl() {
        setClassName(Hotelroom.class.getSimpleName().toLowerCase());
    }

    public static HotelroomDAOImpl getInstance() {
        return instance;
    }

    private final static HotelroomDAOImpl instance = new HotelroomDAOImpl();

    private final static Logger logger = LogManager.getLogger();

    private final static String COMFORT = "comfort";
    private final static String CAPACITY = "capacity";
    private final static String PRICE = "price";
    private final static String HOTEL_NAME = "hotel_name";
    private final static String CITY = "city";
    private final static String URL = "url";
    private final static String NAME = "name";

    private final static String COMMON_HOTELROOM_SHOW
            = "SELECT hotelroom.price, hotelroom.capacity, hotelroom.comfort, hotel.name"
            + " AS 'hotel_name',hotel.city FROM hotelroom JOIN hotel ON hotelroom.hotel = hotel.id";
    private final static String PRICE_FILTER = "WHERE price > ? AND price < ?";
    private final static String COMFORT_FILTER = "WHERE comfort= ? ";

    private final static String ALL_VALID_REQUESTED_HOTELROOMS = "SELECT DISTINCT" +
            "       hotelroom.price       AS price, " +
            "       hotelroom.capacity    AS capacity, " +
            "       hotelroom.comfort     AS comfort, " +
            "       hotelroom.number      AS number, " +
            "       hotelroom.image_url   AS url, " +
            "       hotelroom.description AS description, " +
            "       hotelroom.description_en AS descriptionEn, " +
            "       hotelroom.id          AS hotelroom_id, " +
            "       hotel.city            AS city, " +
            "       hotel.name            AS name, " +
            "       hotel.id              AS hotel_id " +
            "from hotelroom " +
            "       join hotel ON hotelroom.hotel = hotel.id " +
            "       left join   hotel_booking.order ON hotelroom.id = hotel_booking.order.hotelroom " +
            "WHERE (hotelroom.id NOT IN (SELECT hotel_booking.hotelroom.id as id " +
            "                            from hotel_booking.`order` " +
            "                                        left join hotelroom  on `order`.hotelroom = hotelroom.id " +
            "                            where (arrival_data BETWEEN ? and ?) " +
            "                               or (departure_data BETWEEN ? and ?)) " +
            "  OR ( arrival_data IS NULL and departure_data is null)) " +
            "  AND hotel.city = ? " +
            "  AND capacity >= ? " +
            "  LIMIT ?,?;";


    private final static String VALID_HOTELROOMS_AMOUNT = "SELECT DISTINCT  " +
            "count(hotel_booking.hotelroom.id) AS count_id " +
            "from hotelroom " +
            "       join hotel ON hotelroom.hotel = hotel.id " +
            "       left join   hotel_booking.order ON hotelroom.id = hotel_booking.order.hotelroom " +
            "WHERE (hotelroom.id NOT IN (SELECT hotel_booking.hotelroom.id as id " +
            "                            from hotel_booking.`order` " +
            "                                        left join hotelroom  on `order`.hotelroom = hotelroom.id " +
            "                            where (arrival_data BETWEEN ? and ?) " +
            "                               or (departure_data BETWEEN ? and ?)) " +
            "  OR ( arrival_data IS NULL and departure_data is null)) " +
            "  AND hotel.city = ? " +
            "  AND capacity >= ?;";


    private final static String DAYS_AMOUNT = "SELECT DATEDIFF(?, ?) AS days";

    private ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public List<Hotelroom> showAll(HotelSerchDataContainer container, Pagination pagination) {
        List<Hotelroom> hotelroomList = new ArrayList<>();
        int start = (pagination.getCurrentPage() != 1)
                ? (pagination.getCurrentPage() - 1) * Pagination.HOTEL_PER_PAGE
                : 0;
        int finish = start + Pagination.HOTEL_PER_PAGE;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement(ALL_VALID_REQUESTED_HOTELROOMS);
            statement.setString(1, container.getArrivalData());
            statement.setString(2, container.getDepartureData());
            statement.setString(3, container.getArrivalData());
            statement.setString(4, container.getDepartureData());
            statement.setString(5, container.getCity());
            statement.setInt(6, container.getPeopleCapacity());
            statement.setInt(7, start);
            statement.setInt(8, finish);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Hotelroom hotelroom = new Hotelroom();
                hotelroom.setComfort(resultSet.getString(COMFORT));
                hotelroom.setPeopleCapacity(resultSet.getInt(CAPACITY));
                hotelroom.setPrice(resultSet.getInt(PRICE));
                hotelroom.setId(resultSet.getInt("hotelroom_id"));
                hotelroom.setNumber(resultSet.getInt("number"));
                hotelroom.setImageURL(resultSet.getString(URL));
                hotelroom.setDescription(resultSet.getString("description"));
                hotelroom.setDescriptionEn(resultSet.getString("descriptionEn"));
                Hotel hotel = new Hotel();
                hotel.setName(resultSet.getString(NAME));
                hotel.setCity(resultSet.getString(CITY));
                hotel.setId(resultSet.getInt("hotel_id"));
                hotelroom.setHotel(hotel);
                hotelroomList.add(hotelroom);
            }
            return hotelroomList;
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return Collections.emptyList();
    }

    public int getAmountOfValidHotelrooms(HotelSerchDataContainer container) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement
                    = connection.prepareStatement(VALID_HOTELROOMS_AMOUNT);
            statement.setString(1, container.getArrivalData());
            statement.setString(2, container.getDepartureData());
            statement.setString(3, container.getArrivalData());
            statement.setString(4, container.getDepartureData());
            statement.setString(5, container.getCity());
            statement.setInt(6, container.getPeopleCapacity());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("count_id");
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
        return 0;
    }

    @Override
    public int getDayAmount(String arrivalData, String departureData) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement
                    = connection.prepareStatement(DAYS_AMOUNT);
            statement.setString(1, departureData);
            statement.setString(2, arrivalData);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("days");
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return 0;
    }

    private void setHotelroomOutput(ResultSet resultSet, List<Hotelroom> hotelroomList) throws SQLException {
        while (resultSet.next()) {
            Hotelroom hotelroom = new Hotelroom();
            hotelroom.setComfort(resultSet.getString(COMFORT));
            hotelroom.setPeopleCapacity(resultSet.getInt(CAPACITY));
            hotelroom.setPrice(resultSet.getInt(PRICE));
            Hotel hotel = new Hotel();
            hotel.setName(resultSet.getString(HOTEL_NAME));
            hotel.setCity(resultSet.getString(CITY));
            hotelroom.setHotel(hotel);
            hotelroomList.add(hotelroom);
        }
    }
}
