package dao.impl;

import dao.ConnectionPool;
import dao.GenericDAO;
import dao.command.CommandDAO;
import dao.command.DirectorCommandDAO;
import dao.entity.Entity;
import dao.entity.impl.Hotelroom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDAO implements GenericDAO<Entity> {
    private static DirectorCommandDAO directorCommand = DirectorCommandDAO.getInstance();

    private ConnectionPool pool = ConnectionPool.getInstance();

    private final static Logger logger = LogManager.getLogger();

    private String className;

    void setClassName(String className) {
        this.className = className;
    }

    @Override
    public Optional<Entity> findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("SELECT * FROM hotel_booking." + className + " WHERE id=?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            CommandDAO command = directorCommand.getCommand(className);
            return command.findById(resultSet);
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return Optional.empty();
    }

    @Override
    public List<Entity> findAll() {
        List<Entity> entityList;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = pool.getConnection();
            statement
                    = connection.prepareStatement("SELECT * FROM hotel_booking." + className);
            resultSet = statement.executeQuery();
            CommandDAO command = directorCommand.getCommand(className);
            entityList = command.findAll(resultSet);
            return entityList;
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
        return Collections.emptyList();
    }

    @Override
    public boolean delete(Entity entity) {
        Connection connection = null;
        try {
            connection = pool.getConnection();
            CommandDAO command = directorCommand.getCommand(className);
            return command.delete(entity, connection);
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeConnection(connection);
        }
        return false;
    }

    @Override
    public boolean create(Entity entity) {
        Connection connection = null;
        try {
            connection = pool.getConnection();
            CommandDAO command = directorCommand.getCommand(className);
            return command.create(entity, connection);
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeConnection(connection);
        }
        return false;
    }

    @Override
    public Optional<Entity> update(Entity entity) {
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("UPDATE " + className + " ");
            statement.setInt(1, entity.getId());
            if (statement.executeUpdate() == 1) {
                return Optional.of(entity);
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.closeConnection(connection);
        }
        return Optional.empty();
    }
}
