package dao.impl;

import dao.ConnectionPool;
import dao.DiscountDAO;
import dao.entity.UserRole;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.Discount;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class DiscountDAOImpl extends AbstractDAO implements DiscountDAO {
    private final static DiscountDAOImpl instance = new DiscountDAOImpl();

    private DiscountDAOImpl() {
        setClassName(Discount.class.getSimpleName().toLowerCase());
    }

    public static DiscountDAOImpl getInstance() {
        return instance;
    }

    private final static Logger logger = LogManager.getLogger();

    private ConnectionPool pool = ConnectionPool.getInstance();

    private final static String DISCOUNT_LIST = "SELECT * FROM discount";

    @Override
    public List<Discount> getDiscountList() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Discount> discountList = new ArrayList<>();
        try {
            connection = pool.getConnection();
            statement = connection
                    .prepareStatement(DISCOUNT_LIST);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Discount discount = new Discount();
                discount.setUserJobPlace(resultSet.getString("user_job_place"));
                discountList.add(discount);
            }
            return discountList;
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return Collections.emptyList();
    }
}
