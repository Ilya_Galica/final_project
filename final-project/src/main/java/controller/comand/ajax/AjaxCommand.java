package controller.comand.ajax;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AjaxCommand {
    String CITY = "city";
    String ARRIVAL_DATA = "arrival";
    String DEPARTURE_DATA = "departure";
    String PEOPLE_AMOUNT = "amount";
    String CURRENT_PAGE = "currentPage";
    String DATA_CONTEINER = "dataContainer";

    String JSP_PACKAGE = "jsp/";
    String HOTELROOM_PACKAGE = "hotelroom/";
    String ORDER_PACKAGE = "order/";
    void execute(HttpServletRequest request, HttpServletResponse response);
}
