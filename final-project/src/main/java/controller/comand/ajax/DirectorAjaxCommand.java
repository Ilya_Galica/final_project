package controller.comand.ajax;

import controller.comand.ajax.impl.*;

import java.util.HashMap;
import java.util.Map;

public class DirectorAjaxCommand {
    private Map<String, AjaxCommand> map = new HashMap<>();
    private final static DirectorAjaxCommand instance = new DirectorAjaxCommand();

    public static DirectorAjaxCommand getInstance() {
        return instance;
    }

    private DirectorAjaxCommand() {
        map.put("confirmOrder", new ConfirmOrderCommand());
    }

    public AjaxCommand getCommand(String commandKey) {
        return map.get(commandKey);
    }
}
