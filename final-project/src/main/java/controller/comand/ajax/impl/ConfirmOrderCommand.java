package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import dao.entity.impl.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.nio.charset.StandardCharsets;
import java.util.Properties;

import static controller.util.ControllerUtil.encoding;


public class ConfirmOrderCommand implements AjaxCommand {
    private final static Logger logger = LogManager.getLogger();

    private static final String SENDER_EMAIL_ADDRESS = "NurmagomedovMagomed2000@gmail.com";
    private static final String SENDER_EMAIL_PASSWORD = "Nurmagomedov_Magomed_2000";
    private static final String SENDER_HOST = "smtp.gmail.com";
    private static final String SENDER_PORT = "587";
    private static final String PROTOCOL = "smtp";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        Order order = (Order) request.getSession().getAttribute("order");

        String subject = "Бронь отеля на сайте ONEDIRECTION";

        String responseText = "Здравствуйте дорогой " + order.getUser().getName() + " " + order.getUser().getPatronymic()
                + ",\n ваш заказ комнаты в период с " + order.getArrivalData() + " по " + order.getDepartureData()
                + " успешно осуществлен.\n Отель " + order.getHotelroom().getHotel().getName() + ", комната №"
                + order.getHotelroom().getNumber() + ".\n Общая сумма заказа составляет:" + order.getTotalPrice()
                + "$.\n Удачной вам поездки ";

        String receiverEmailAddress = order.getUser().getEmail();

        Properties properties = getProperties();

        Session session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(SENDER_EMAIL_ADDRESS, SENDER_EMAIL_PASSWORD);
                    }
                });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SENDER_EMAIL_ADDRESS));
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(receiverEmailAddress));
            message.setSubject(subject);
            message.setText(responseText);
            Transport transport = session.getTransport();
            transport.connect(SENDER_EMAIL_ADDRESS, SENDER_EMAIL_PASSWORD);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            String language = (String) request.getSession().getAttribute("language");
            String json;
            if (language == null || language.equals("ru")) {
                json = new Gson().toJson(
                        decoding(order.getUser().getName()
                                + " " + order.getUser().getSurname()
                                + ", ваше письмо было отправлено "));
            } else {
                json = new Gson().toJson(
                        order.getUser().getName()
                                + " " + order.getUser().getSurname()
                                + ", your letter was send");
            }
            response.getWriter().write(json);
        } catch (IOException | MessagingException e) {
            logger.error(e);
        }
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        properties.put("mail.transport.protocol", PROTOCOL);
        properties.put("mail.smtp.host", SENDER_HOST);
        properties.put("mail.smtp.port", SENDER_PORT);
        properties.put("mail.from", SENDER_EMAIL_ADDRESS);
        properties.put("mail.smtp.password", SENDER_EMAIL_PASSWORD);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        return properties;
    }

    private static String decoding(String param) {
        return new String(param.getBytes(StandardCharsets.UTF_8),
                StandardCharsets.ISO_8859_1);
    }

}
