package controller.comand.java;

import controller.comand.java.impl.*;

import java.util.HashMap;
import java.util.Map;

class DirectorCommand {
    private Map<String, Command> map = new HashMap<>();
    private final static DirectorCommand instance = new DirectorCommand();

    static DirectorCommand getInstance() {
        return instance;
    }

    private DirectorCommand() {
        map.put("searchHotels", new SearchHotelCommand());
        map.put("pagination", new SearchHotelCommand());
        map.put("hotelroomPage", new ShowHotelroomPageCommand());
        map.put("profileExit", new ProfileExitCommand());
        map.put("profileEnter", new ProfileEnterCommand());
        map.put("signIn", new SignInCommand());
        map.put("signUp", new SignUpCommand());
        map.put("changeData", new ChangePersonalDataCommand());
        map.put("denyOrder", new DenyOrderCommand());
        map.put("changeLanguage", new ChangeLanguageCommand());
        map.put("createOrder", new CreateOrderCommand());
        map.put("findAllUserAdmin", new FindAllRecordAdminCommand());
        map.put("findAllDiscountAdmin", new FindAllRecordAdminCommand());
        map.put("findAllOrderAdmin", new FindAllRecordAdminCommand());
        map.put("findAllHotelAdmin", new FindAllRecordAdminCommand());
        map.put("findAllHotelroomAdmin", new FindAllRecordAdminCommand());
        map.put("findDiscountAdmin", new FindRecordAdminCommand());
        map.put("findUserAdmin", new FindRecordAdminCommand());
        map.put("findHotelAdmin", new FindRecordAdminCommand());
        map.put("findOrderAdmin", new FindRecordAdminCommand());
        map.put("findHotelroomAdmin", new FindRecordAdminCommand());
        map.put("deleteUserAdmin", new DeleteRecordAdminCommand());
        map.put("deleteDiscountAdmin", new DeleteRecordAdminCommand());
        map.put("deleteHotelroomAdmin", new DeleteRecordAdminCommand());
        map.put("deleteOrderAdmin", new DeleteRecordAdminCommand());
        map.put("deleteHotelAdmin", new DeleteRecordAdminCommand());
        map.put("createDiscountAdmin", new CreateRecordAdminCommand());
        map.put("createHotelroomAdmin", new CreateRecordAdminCommand());
        map.put("createHotelAdmin", new CreateRecordAdminCommand());
    }

    Command getCommand(String commandKey) {
        return map.get(commandKey);
    }
}
