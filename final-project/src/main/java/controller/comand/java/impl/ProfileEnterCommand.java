package controller.comand.java.impl;

import controller.comand.java.Command;

import javax.servlet.http.HttpServletRequest;

public class ProfileEnterCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        return JSP_PACKAGE+"user/personalUserPage";
    }
}
