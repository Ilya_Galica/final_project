package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.entity.Entity;
import dao.entity.impl.User;
import dao.impl.AbstractDAO;
import dao.impl.UserDAOImpl;
import service.*;
import service.impl.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class FindAllRecordAdminCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        String command = request.getParameter("command");
        switch (command) {
            case "findAllUserAdmin":
                UserService userService = UserServiceImpl.getInstance();
                List<Entity> userList = userService.findAll();
                httpSession.setAttribute("users", userList);
                break;
            case "findAllDiscountAdmin":
                DiscountService discountService = DiscountServiceImpl.getInstance();
                List<Entity> discountServiceAll = discountService.findAll();
                httpSession.setAttribute("discounts", discountServiceAll);
                break;
            case "findAllOrderAdmin":
                OrderService orderService = OrderServiceImpl.getInstance();
                List<Entity> orderList = orderService.findAll();
                httpSession.setAttribute("orders", orderList);
                break;
            case "findAllHotelAdmin":
                HotelService hotelService = HotelServiceImpl.getInstance();
                List<Entity> hotelList = hotelService.findAll();
                httpSession.setAttribute("hotels", hotelList);
                break;
            case "findAllHotelroomAdmin":
                HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
                List<Entity> holetroomList = hotelroomService.findAll();
                httpSession.setAttribute("holetroomList", holetroomList);
                break;
        }

        return "/WEB-INF/admin/admin";
    }

}
