package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;
import service.exception.ServiceException;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class SignInCommand implements Command {
    private final static Logger logger = LogManager.getLogger();

    @Override
    public String execute(HttpServletRequest request) {
        String pageName = request.getParameter(PAGE_NAME);
        String email = request.getParameter("loginEmail");
        String password = request.getParameter(PASSWORD);
        try {
            UserService userService = UserServiceImpl.getInstance();
            User user = userService.signIn(email, password);
            request.getSession().setAttribute(USER, user);
            if (ADMIN.equalsIgnoreCase(String.valueOf(user.getRole()))) {
                return "WEB-INF/admin/admin";
            } else {
                return pageName;
            }
        } catch (SuchUserNotFoundException | ServiceException e) {
            logger.error(e);
            return pageName;
        }
    }
}
