package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.OrderDAO;
import dao.entity.impl.Order;
import dao.impl.AbstractDAO;
import dao.impl.OrderDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.OrderService;
import service.exception.ServiceException;
import service.impl.OrderServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class DenyOrderCommand implements Command {
    private final static Logger logger = LogManager.getLogger();

    @Override
    public String execute(HttpServletRequest request) {
        Order order = (Order) request.getSession().getAttribute(ORDER);
        OrderService service = OrderServiceImpl.getInstance();
        try {
            service.deleteOrder(order);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
        }
        return JSP_PACKAGE + "showRequestHotels";
    }
}
