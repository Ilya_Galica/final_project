package controller.comand.java.impl;

import controller.comand.java.Command;

import javax.servlet.http.HttpServletRequest;

public class ProfileExitCommand implements Command {

    public String execute(HttpServletRequest request) {
        String pageName = request.getParameter("pageName");
        request.getSession().removeAttribute("user");
        return pageName;
    }
}
