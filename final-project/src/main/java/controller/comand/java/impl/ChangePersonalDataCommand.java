package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;
import service.exception.ServiceException;
import service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangePersonalDataCommand implements Command {
    private final static String USER = "user";
    private final static String EMAIL = "email";
    private final static String PASSWORD = "password";
    private final static String TELEPHONE = "telephone";
    private final static String JOB = "job";

    private final static Logger logger = LogManager.getLogger();

    @Override
    public String execute(HttpServletRequest request) {
        String param;
        UserService userService = UserServiceImpl.getInstance();
        HttpSession httpSession = request.getSession();
        User user = (User) httpSession.getAttribute(USER);
        try {
            if ((param = request.getParameter(EMAIL))!=null) {
                user = userService.changePersonalInfo(param, EMAIL, user);
            } else if ((param = request.getParameter(PASSWORD))!= null) {
                user = userService.changePersonalInfo(param, PASSWORD, user);
            } else if ((param = request.getParameter(TELEPHONE))!= null) {
                user = userService.changePersonalInfo(param, TELEPHONE, user);
            } else {
                param = request.getParameter(JOB);
                user = userService.changePersonalInfo(param, JOB, user);
            }
            httpSession.removeAttribute("user");
            httpSession.setAttribute("user", user);
//            User us = (User) httpSession.getAttribute("user");
        } catch (ServiceException e) {
            logger.error(e);
        }
        return JSP_PACKAGE + "user/personalUserPage";
    }
}
