package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.entity.Entity;
import dao.entity.impl.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.*;
import service.exception.ServiceException;
import service.impl.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class FindRecordAdminCommand implements Command {
    private final static Logger logger = LogManager.getLogger();

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        String id = request.getParameter("findId");
        String command = request.getParameter("command");
        switch (command) {
            case "findUserAdmin":
                User user = null;
                try {
                    UserService userService = UserServiceImpl.getInstance();
                    user = (User) userService.findById(Integer.parseInt(id));
                } catch (ServiceException e) {
                    logger.error(e.getMessage());
                    httpSession.setAttribute("findUser", "Не нашли по такому id = " + id);
                }
                httpSession.setAttribute("findUser", user);
                break;
            case "findHotelAdmin":
                Hotel hotel = null;
                try {
                    HotelService hotelService = HotelServiceImpl.getInstance();
                    hotel = (Hotel) hotelService.findById(Integer.parseInt(id));
                } catch (ServiceException e) {
                    logger.error(e.getMessage());
                    httpSession.setAttribute("findHotel", "Не нашли по такому id = " + id);
                }
                httpSession.setAttribute("findHotel", hotel);
                break;
            case "findOrderAdmin":
                Order order = null;
                try {
                    OrderService orderService = OrderServiceImpl.getInstance();
                    order = (Order) orderService.findById(Integer.parseInt(id));
                } catch (ServiceException e) {
                    logger.error(e.getMessage());
                    httpSession.setAttribute("findOrder", "Не нашли по такому id = " + id);
                }
                httpSession.setAttribute("findOrder", order);
                break;
            case "findHotelroomAdmin":
                Hotelroom hotelroom = null;
                try {
                    HotelroomService hotelService = HotelroomServiceImpl.getInstance();
                    hotelroom = (Hotelroom) hotelService.findById(Integer.parseInt(id));
                } catch (ServiceException e) {
                    logger.error(e.getMessage());
                    httpSession.setAttribute("findHotelroom", "Не нашли по такому id = " + id);
                }
                httpSession.setAttribute("findHotelroom", hotelroom);
                break;
        }
        return "/WEB-INF/admin/admin";
    }
}
