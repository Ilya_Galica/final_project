package controller.comand.java.impl;

import controller.comand.java.Command;
import controller.util.ControllerUtil;
import dao.entity.HotelSerchDataContainer;
import dao.entity.impl.Hotelroom;
import service.HotelroomService;
import service.impl.HotelroomServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ShowHotelroomPageCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        String hotelroomPosition = request.getParameter("hotelroomPosition");
        @SuppressWarnings("unchecked")
        List<Hotelroom> hotelroomList = (List<Hotelroom>) request.getSession().getAttribute(HOTELROOMS);
        Hotelroom hotelroom = hotelroomList.get(Integer.parseInt(hotelroomPosition));
        HotelSerchDataContainer container
                = (HotelSerchDataContainer) request.getSession().getAttribute(DATA_CONTAINER);
        HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
        String arrivalData = container.getArrivalData();
        String departureData = container.getDepartureData();
        Integer dayAmount
                = hotelroomService
                .getDayAmount(ControllerUtil.reverseDateForSQL(arrivalData),
                        ControllerUtil.reverseDateForSQL(departureData));
        request.getSession().setAttribute(HOTELROOM, hotelroom);
        request.setAttribute(DAY_AMOUNT, dayAmount);
        return "/" + JSP_PACKAGE + HOTELROOM_PACKAGE + "showHotelroomPage";
    }
}
