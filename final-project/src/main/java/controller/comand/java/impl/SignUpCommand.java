package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;
import service.exception.ServiceException;
import service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;

import static controller.util.ControllerUtil.encoding;

public class SignUpCommand implements Command {
    private final static Logger logger = LogManager.getLogger();

    @Override
    public String execute(HttpServletRequest request) {
        String pageName = request.getParameter(PAGE_NAME);
        User user = new User();
        user.setName(request.getParameter(NAME));
        user.setSurname(request.getParameter(SURNAME));
        user.setPatronymic(request.getParameter(PATRONYMIC));
        user.setEmail(request.getParameter(EMAIL));
        user.setTelephone(request.getParameter(TELEPHONE));
        user.setPassword(request.getParameter(PASSWORD));
        user.setJobPlace(request.getParameter(JOB_PLACE));
        UserService userService = UserServiceImpl.getInstance();
        try {
            if (userService.signUp(user)) {
                request.getSession().setAttribute(USER, user);
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return pageName;
        }

        return pageName;
    }


}