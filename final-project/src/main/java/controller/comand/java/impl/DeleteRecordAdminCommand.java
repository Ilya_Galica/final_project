package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.entity.Entity;
import dao.entity.impl.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.*;
import service.exception.ServiceException;
import service.impl.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class DeleteRecordAdminCommand implements Command {
    private final static Logger logger = LogManager.getLogger();

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        String id = request.getParameter("deleteId");
        String command = request.getParameter("command");
        boolean isDelete;
        switch (command) {
            case "deleteUserAdmin":
                User user = new User();
                user.setId(Integer.parseInt(id));
                UserService userService = UserServiceImpl.getInstance();
                isDelete = userService.delete(user);
                httpSession.setAttribute("deleteUser", (isDelete)
                        ? " Пользователь с id = " + id + " удален"
                        : " ПРоверьте введенный id");
                break;
            case "deleteDiscountAdmin":
                Discount discount = new Discount();
                discount.setId(Integer.parseInt(id));
                DiscountService discountService = DiscountServiceImpl.getInstance();
                isDelete = discountService.delete(discount);
                httpSession.setAttribute("deleteDiscount", (isDelete)
                        ? " Скидка с с id = " + id + " удалена"
                        : " Проверьте введенный id");
            case "deleteHotelroomAdmin":
                Hotelroom hotelroom = new Hotelroom();
                hotelroom.setId(Integer.parseInt(id));
                HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
                isDelete = hotelroomService.delete(hotelroom);
                httpSession.setAttribute("deleteHotelroom", (isDelete)
                        ? " Комната с с id = " + id + " удалена"
                        : " Проверьте введенный id");
            case "deleteOrderAdmin":
                Order order = new Order();
                order.setId(Integer.parseInt(id));
                OrderService orderService = OrderServiceImpl.getInstance();
                isDelete = orderService.delete(order);
                httpSession.setAttribute("deleteOrder", (isDelete)
                        ? " Заказ с с id = " + id + " удален"
                        : " Проверьте введенный id");
            case "deleteHotelAdmin":
                Hotel hotel = new Hotel();
                hotel.setId(Integer.parseInt(id));
                HotelService hotelService = HotelServiceImpl.getInstance();
                isDelete = hotelService.delete(hotel);
                httpSession.setAttribute("deleteHotel", (isDelete)
                        ? " Отель с с id = " + id + " удален"
                        : " Проверьте введенный id");
        }
        return "/WEB-INF/admin/admin";
    }
}
