package controller.comand.java.impl;

import controller.comand.java.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ChangeLanguageCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        String pageName = request.getParameter("pageName");
        String ru = request.getParameter("localeRU");
        String en = request.getParameter("localeEN");
        String language = (ru == null) ? en : ru;
        HttpSession languageSession = request.getSession();
        if (languageSession.getAttribute("locale") == null
                || !languageSession.getAttribute("locale").equals(language)) {
            languageSession.setAttribute("language", language);
        }
        return pageName;
    }
}
