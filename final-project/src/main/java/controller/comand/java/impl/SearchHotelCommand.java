package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.entity.HotelSerchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotelroom;
import service.HotelroomService;
import service.impl.HotelroomServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static controller.util.ControllerUtil.encoding;

public class SearchHotelCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
        HotelSerchDataContainer container;
        Pagination pagination;
        HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
        if (isRequestAttrInitialised(request)) {
            container = setConteinerData(request);
            pagination = setPagination(request, container, hotelroomService);
        } else {
            container = (HotelSerchDataContainer) request.getSession().getAttribute(DATA_CONTAINER);
            pagination = (Pagination) request.getSession().getAttribute(PAGINATION);
            pagination.setCurrentPage((request.getParameter(CURRENT_PAGE) != null)
                    ? Integer.parseInt(request.getParameter(CURRENT_PAGE))
                    : 1);
        }

        List<Hotelroom> hotelroomList = null;
        if (container != null) {
            hotelroomList =
                    hotelroomService.showAll(container, pagination);
            container.setArrivalData(reverseDateFromSQL(container.getArrivalData()));
            container.setDepartureData(reverseDateFromSQL(container.getDepartureData()));
        }
        request.getSession().setAttribute(DATA_CONTAINER, container);
        request.getSession().setAttribute(HOTELROOMS, hotelroomList);
        request.getSession().setAttribute(PAGINATION, pagination);

        return JSP_PACKAGE + "showRequestHotels";
    }


    private Pagination setPagination(HttpServletRequest request,
                                     HotelSerchDataContainer container,
                                     HotelroomService hotelroomService) {
        Pagination pagination = new Pagination();
        pagination
                .setCurrentPage((request.getParameter(CURRENT_PAGE) != null)
                        ? Integer.parseInt(request.getParameter(CURRENT_PAGE))
                        : 1);
        pagination
                .setAllPageAmount(hotelroomService
                        .getAmountOfValidHotelrooms(container) - 1);
        int paginationAmount = (pagination.getAllPageAmount() % Pagination.HOTEL_PER_PAGE != 0)
                ? (pagination.getAllPageAmount() / Pagination.HOTEL_PER_PAGE) + 1
                : pagination.getAllPageAmount() / Pagination.HOTEL_PER_PAGE;
        pagination.setPaginationAmount(paginationAmount);
        return pagination;
    }

    private HotelSerchDataContainer setConteinerData(HttpServletRequest request) {
        HotelSerchDataContainer container = new HotelSerchDataContainer();
        container.setCity(encoding(request.getParameter(CITY)));
        container.setArrivalData(request.getParameter(ARRIVAL_DATA));
        container.setDepartureData(request.getParameter(DEPARTURE_DATA));
        container.setPeopleCapacity(Integer.parseInt(request.getParameter(PEOPLE_AMOUNT)));
        return container;
    }

    private boolean isRequestAttrInitialised(HttpServletRequest request) {
        return request.getParameter(CITY) != null
                && request.getParameter(ARRIVAL_DATA) != null
                && request.getParameter(DEPARTURE_DATA) != null
                && request.getParameter(PEOPLE_AMOUNT) != null;
    }

    private static String reverseDateFromSQL(String date) {
        return date.replaceAll("((\\d)*)-((\\d)*)-((\\d)*)", "$5/$3/$1");
    }
}
