package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.entity.HotelSerchDataContainer;
import dao.entity.impl.Hotelroom;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.OrderService;
import service.exception.ServiceException;
import service.impl.OrderServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

import static controller.util.ControllerUtil.encoding;
import static controller.util.ControllerUtil.reverseDateForSQL;

public class CreateOrderCommand implements Command {
    private final static Logger logger = LogManager.getLogger();

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        HotelSerchDataContainer container
                = (HotelSerchDataContainer) request.getSession()
                .getAttribute(DATA_CONTAINER);
        Hotelroom hotelroom
                = (Hotelroom) request.getSession()
                .getAttribute(HOTELROOM);
        User user = new User();
        if (request.getSession().getAttribute(USER) != null) {
            user = (User) request.getSession().getAttribute(USER);
        } else {
            user.setName(encoding(request.getParameter(NAME)));
            user.setSurname(encoding(request.getParameter(SURNAME)));
            user.setPatronymic(encoding(request.getParameter(PATRONYMIC)));
            user.setEmail(request.getParameter(EMAIL));
            user.setTelephone(request.getParameter(TELEPHONE));
            user.setJobPlace(encoding(request.getParameter(JOB_PLACE)));
        }
        Order order = new Order();
        order.setArrivalData(container.getArrivalData());
        order.setDepartureData(container.getDepartureData());
        order.setUser(user);
        order.setHotelroom(hotelroom);
        OrderService orderService = OrderServiceImpl.getInstance();
        try {
            if (orderService.createOrder(order).isPresent()) {
                order = orderService.createOrder(order).get();
                session.setAttribute(ORDER, order);
            } else {
                session.setAttribute(ORDER, "А нет его");
            }
        } catch (RuntimeException e) {
            logger.error(e);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
        }
        return JSP_PACKAGE + ORDER_PACKAGE + "confirmOrderPage";
    }
}
