package controller.comand.java.impl;

import controller.comand.java.Command;
import dao.entity.impl.Discount;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import dao.entity.impl.User;
import service.DiscountService;
import service.HotelService;
import service.HotelroomService;
import service.UserService;
import service.impl.DiscountServiceImpl;
import service.impl.HotelServiceImpl;
import service.impl.HotelroomServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static controller.util.ControllerUtil.encoding;

public class CreateRecordAdminCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        String command = request.getParameter("command");
        boolean isCreate;
        switch (command) {
            case "createDiscountAdmin":
                Discount discount = new Discount();
                discount.setUserJobPlace(encoding(request.getParameter("insertUserJobPlace")));
                discount.setDiscountValueForJobPlace(Integer
                        .parseInt(request.getParameter("insertDiscountValueForJobPlace")));
                DiscountService discountService = DiscountServiceImpl.getInstance();
                isCreate = discountService.create(discount);
                httpSession.setAttribute("createDiscount", (isCreate)
                        ? " Скидка добавлена"
                        : " Проверьте что ввели");
                break;
            case "createHotelroomAdmin":
                Hotelroom hotelroom = new Hotelroom();
                hotelroom.setNumber(Integer.parseInt(request.getParameter("insertNumber")));
                Hotel hotelroomHotel = new Hotel();
                hotelroomHotel.setId(Integer.parseInt(request.getParameter("insertHotelId")));
                hotelroom.setHotel(hotelroomHotel);
                hotelroom.setPrice(Integer.parseInt(request.getParameter("insertPrice")));
                hotelroom.setPeopleCapacity(Integer.parseInt(request.getParameter("insertCapacity")));
                hotelroom.setComfort(request.getParameter("insertComfort"));
                hotelroom.setImageURL(encoding(request.getParameter("insertImgURL")));
                hotelroom.setDescription(encoding(request.getParameter("insertDescription")));
                hotelroom.setDescriptionEn(request.getParameter("insertDescriptionEn"));
                HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
                isCreate = hotelroomService.create(hotelroom);
                httpSession.setAttribute("createHotelroom", (isCreate)
                        ? " Комната добавлена"
                        : " Проверьте что ввели");
                break;
            case "createHotelAdmin":
                Hotel hotel = new Hotel();
                hotel.setName(encoding(request.getParameter("insertHotelName")));
                hotel.setCity(encoding(request.getParameter("insertCity")));
                hotel.setCountry(encoding(request.getParameter("insertCountry")));
                HotelService hotelService = HotelServiceImpl.getInstance();
                isCreate = hotelService.create(hotel);
                httpSession.setAttribute("createHotel", (isCreate)
                        ? " Отель добавлен"
                        : " Проверьте что ввели");
                break;
        }
        return "/WEB-INF/admin/admin";
    }
}
