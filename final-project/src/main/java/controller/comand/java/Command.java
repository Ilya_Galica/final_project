package controller.comand.java;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    String CITY = "city";
    String ARRIVAL_DATA = "arrival";
    String DEPARTURE_DATA = "departure";
    String PEOPLE_AMOUNT = "amount";
    String CURRENT_PAGE = "currentPage";
    String DATA_CONTAINER = "dataContainer";
    String USER = "user";
    String ADMIN = "admin";
    String DAY_AMOUNT = "dayAmount";
    String PAGINATION = "pagination";

    String NAME = "name";
    String SURNAME = "surname";
    String PATRONYMIC = "patronymic";
    String EMAIL = "email";
    String PASSWORD = "password";
    String TELEPHONE = "telephone";
    String JOB_PLACE = "jobPlace";
    String HOTELROOM = "hotelroom";
    String HOTELROOMS = "hotelrooms";
    String ORDER = "order";
    String PAGE_NAME = "pageName";

    String JSP_PACKAGE = "jsp/";
    String HOTELROOM_PACKAGE = "hotelroom/";
    String ORDER_PACKAGE = "order/";
    String USER_PACKAGE = "user/";

    String execute(HttpServletRequest request);
}
