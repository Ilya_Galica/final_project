package controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;


public class RequestWrapper extends HttpServletRequestWrapper implements Serializable {

    private static final Logger logger = LogManager.getLogger(RequestWrapper.class);
    private static String body;

    public void setBody(String body) {
        RequestWrapper.body = body;
    }

    public String getBody(){
        return body;
    }



    @Override
    public String getParameter(String name) {
            String[] requestBodyArray = body.split("&");
            for (String aRequestBodyArray : requestBodyArray) {
                if (aRequestBodyArray.contains(name + "=")) {
                    return (aRequestBodyArray.split("=").length==2)
                    ?aRequestBodyArray.split("=")[1]
                    :"";
                }
            }
        return null;
    }

    public RequestWrapper(HttpServletRequest request) {
        super(request);

        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();

            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                char[] charBuffer = new char[300];
                int bytesRead;

                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            }
        } catch (IOException ex) {
            logger.error("Error reading the request body...");
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    logger.error("Error closing bufferedReader...");
                }
            }
        }

        try {
            body = URLDecoder.decode(stringBuilder.toString(),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error(e);
        }
        System.out.println(body + " bod");
    }

    @Override
    public ServletInputStream getInputStream() {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(body.getBytes());

        return new ServletInputStream() {

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }

            public int read() {
                return byteArrayInputStream.read();
            }
        };
    }
}
