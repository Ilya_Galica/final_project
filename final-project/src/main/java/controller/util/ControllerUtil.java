package controller.util;

import java.nio.charset.StandardCharsets;

public class ControllerUtil {
    public static String reverseDateForSQL(String date) {
        return date
                .replaceAll("((\\d)*)/((\\d)*)/((\\d)*)", "$5-$3-$1");
    }
    public static String encoding(String param){
        return new String(param.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }
}
