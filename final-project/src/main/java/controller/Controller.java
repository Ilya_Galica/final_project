package controller;

import controller.comand.ajax.AjaxCommand;
import controller.comand.ajax.DirectorAjaxCommand;
import controller.comand.java.ActionFactoryCommand;
import dao.entity.impl.Hotel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/controller", name = "controller")
public class Controller extends HttpServlet {
    private final static String COMMAND = "command";
    private final static String AJAX_COMMAND = "ajaxCommand";
    private final static String JSP = ".jsp";
    private String lastCommand;
    private String lastPage;
    private RequestWrapper lastReq;
    private boolean isRedirected;
    private Hotel hotel;
    private final static Logger logger = LogManager.getLogger();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(3 + " req " + req.getMethod());
        processRequestCommand(req, resp);
//        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(1);
        if (req.getParameter(AJAX_COMMAND) != null) {
            processRequestAjaxCommand(req, resp);
        } else if (req.getParameter(COMMAND) == null) {
            resp.sendRedirect("controller");
        }
    }


    private void processRequestCommand(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userCommand = request.getParameter(COMMAND);
//        userCommand = (lastReq.getParameter(COMMAND) == null)
//                ? lastCommand
//                : lastReq.getParameter(COMMAND);
        String resultPage = ActionFactoryCommand.getForwardPage(userCommand, request) + JSP;
        request.getRequestDispatcher(resultPage).forward(request, response);

    }

    private void processRequestAjaxCommand(HttpServletRequest request, HttpServletResponse response) {
        String userCommand;
        try {
            userCommand = request.getParameter(AJAX_COMMAND);
            DirectorAjaxCommand directorCommand = DirectorAjaxCommand.getInstance();
            AjaxCommand command = directorCommand.getCommand(userCommand);
            command.execute(request, response);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void cloneReq(HttpServletRequest request) throws IOException {
        lastReq = new RequestWrapper(request);

    }

    private boolean redirectCondition(HttpServletRequest req) {
        return req.getParameter(COMMAND).equals("denyOrder")
                || req.getParameter(COMMAND).equals("createOrder")
                || req.getParameter(COMMAND).contains("delete")
                || req.getParameter(COMMAND).contains("insert");
    }
}
