package controller.filter;

import controller.RequestWrapper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@WebFilter(filterName = "DuplicateExitRequestFilter", urlPatterns = "/controller")
public class DuplicateExitRequestFilter implements Filter {
    private static RequestWrapper lastRequest;
    private static String lastCommand;
    private static int count = 0;
    private static String lastMethod;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    private static final String[] IP_HEADER_CANDIDATES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR" };

    public static String getClientIpAddress(HttpServletRequest request) {
        for (String header : IP_HEADER_CANDIDATES) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        return request.getRemoteAddr();
    }
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println(getClientIpAddress((HttpServletRequest) servletRequest));
        if (((HttpServletRequest) servletRequest).getMethod().equals("POST")) {
            System.out.println("first post");
            count++;
            lastRequest = new RequestWrapper((HttpServletRequest) servletRequest);
//            System.out.println(((HttpServletRequest) servletRequest).getRequestURI() + "     req POSt");
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            if (servletRequest.getParameter("command") == null && count != 0) {
                System.out.println("then get");
//                System.out.println(lastRequest.getRequestURI()+"    GET"+lastRequest.getMethod() +"    "+lastRequest.getParameter("command"));

//                Map<String, String> map = new HashMap<>();
//
//                Enumeration headerNames = ((HttpServletRequest) servletRequest).getHeaderNames();
//                while (headerNames.hasMoreElements()) {
//                    String key = (String) headerNames.nextElement();
//                    String value = ((HttpServletRequest) servletRequest).getHeader(key);
//                    map.put(key, value);
//                }
//                System.out.println(map);
                System.out.println(lastRequest.getRequestURI());
                if (lastRequest.getRequestURI() == null) {
                    String body = lastRequest.getBody();
                    RequestWrapper localRequest = new RequestWrapper((HttpServletRequest) servletRequest);
                    localRequest.setBody(body);
                    filterChain.doFilter(localRequest, servletResponse);
                } else {
                    filterChain.doFilter(lastRequest, servletResponse);
                }
            } else {
                System.out.println("another");
                count = 0;
                filterChain.doFilter(servletRequest, servletResponse);
            }
        }
    }

    @Override
    public void destroy() {

    }
}
